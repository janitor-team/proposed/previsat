<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>Afficher</name>
    <message>
        <location filename="afficher.ui" line="68"/>
        <source>Fichier texte</source>
        <translation>Text file</translation>
    </message>
    <message>
        <location filename="afficher.ui" line="143"/>
        <source>Prévisions</source>
        <translation>Predictions</translation>
    </message>
    <message>
        <location filename="afficher.ui" line="211"/>
        <source>Satellite</source>
        <translation>Satellite</translation>
    </message>
    <message>
        <location filename="afficher.ui" line="216"/>
        <source>Date de début</source>
        <translation>Start date</translation>
    </message>
    <message>
        <location filename="afficher.ui" line="221"/>
        <source>Date de fin</source>
        <translation>Finish date</translation>
    </message>
    <message>
        <location filename="afficher.ui" line="226"/>
        <source>Hauteur Max</source>
        <translation>Max Elevation</translation>
    </message>
    <message>
        <location filename="afficher.ui" line="231"/>
        <location filename="afficher.cpp" line="327"/>
        <source>Magn</source>
        <translation>Magn</translation>
    </message>
    <message>
        <location filename="afficher.ui" line="236"/>
        <source>Mir</source>
        <translation>Mir</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="300"/>
        <source>Corps</source>
        <translation>Body</translation>
    </message>
    <message>
        <location filename="afficher.ui" line="241"/>
        <source>Hauteur Soleil</source>
        <translation>Sun Elevation</translation>
    </message>
    <message>
        <location filename="afficher.ui" line="271"/>
        <source>about:blank</source>
        <translation></translation>
    </message>
    <message>
        <location filename="afficher.ui" line="292"/>
        <source>Nord</source>
        <translation>North</translation>
    </message>
    <message>
        <location filename="afficher.ui" line="314"/>
        <source>Sud</source>
        <translation>South</translation>
    </message>
    <message>
        <location filename="afficher.ui" line="336"/>
        <source>Est</source>
        <translation>East</translation>
    </message>
    <message>
        <location filename="afficher.ui" line="358"/>
        <source>Ouest</source>
        <translation>West</translation>
    </message>
    <message>
        <location filename="afficher.ui" line="389"/>
        <location filename="afficher.ui" line="392"/>
        <source>Enregistrer</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="244"/>
        <source>Prévisions de passage</source>
        <translation>Predictions of passes</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="248"/>
        <source>Flashs Iridium</source>
        <translation>Iridium flares</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="257"/>
        <source>Transits ISS</source>
        <translation>ISS transits</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="253"/>
        <source>Évènements orbitaux</source>
        <translation>Orbital events</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="261"/>
        <source>Flashs MetOp et SkyMed</source>
        <translation>MetOp and SkyMed flares</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="298"/>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="299"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="324"/>
        <location filename="afficher.cpp" line="325"/>
        <location filename="afficher.cpp" line="345"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="326"/>
        <source>Hauteur Sat</source>
        <translation>Sat Elev</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="328"/>
        <source>Haut Soleil</source>
        <translation>Sun Elev</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="335"/>
        <source>Iridium</source>
        <translation>Iridium</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="424"/>
        <location filename="afficher.cpp" line="428"/>
        <source>W</source>
        <translation>W</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="426"/>
        <location filename="afficher.cpp" line="430"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="464"/>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="464"/>
        <location filename="afficher.cpp" line="465"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="464"/>
        <source>ft</source>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="468"/>
        <source>Longitude</source>
        <translation>Longitude</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="469"/>
        <source>Latitude</source>
        <translation>Latitude</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="469"/>
        <source>Altitude</source>
        <translation>Altitude</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="540"/>
        <source>Enregistrer sous...</source>
        <translation>Save as...</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="541"/>
        <source>Fichiers texte (*.txt);;Tous les fichiers (*)</source>
        <translation>Text files (*.txt);;All files (*)</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="557"/>
        <source>Enregistrer sous</source>
        <translation>Save as</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="558"/>
        <source>Fichiers PNG (*.png);;Fichiers JPEG (*.jpg);;Fichiers BMP (*.bmp);;Tous les fichiers (*)</source>
        <translation>PNG files (*.png);;JPEG files (*.jpg);;BMP files (*.bmp);;All files (*)</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="1088"/>
        <source>Flash Iridium</source>
        <translation>Iridium flare</translation>
    </message>
    <message>
        <location filename="afficher.cpp" line="1094"/>
        <source>Flash %1</source>
        <translation>%1 flare</translation>
    </message>
</context>
<context>
    <name>Apropos</name>
    <message>
        <location filename="apropos.ui" line="147"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="apropos.ui" line="160"/>
        <source>PreviSat est un logiciel de suivi des satellites artificiels afin de les observer.

 Il permet d&apos;afficher la position des satellites en temps réel ou en mode manuel. PreviSat est capable d&apos;effectuer les prévisions de passage, de calculer les flashs Iridium, ainsi que plusieurs autres calculs de prévisions.

PreviSat est entièrement gratuit!</source>
        <oldsource>PreviSat est un logiciel de calcul de la position des satellites artificiels.
Il permet d&apos;afficher la position des satellites à tout instant (en temps réel ou en mode manuel), d&apos;effectuer les prévisions de passage pour n&apos;importe quel lieu d&apos;observation, et également de déterminer très rapidement les flashs Iridium.

PreviSat est entièrement gratuit!</oldsource>
        <translation>PreviSat is a satellite tracking software for observing purposes.

It shows positions of artificial satellites in real time or manual mode. PreviSat is able to make predictions of their passes, predictions of Iridium flares and several other calculations.

PreviSat is free !</translation>
    </message>
    <message>
        <location filename="apropos.cpp" line="75"/>
        <source>À propos de %1 %2</source>
        <translation>About %1 %2</translation>
    </message>
    <message>
        <location filename="apropos.cpp" line="106"/>
        <source>Version %1  (%2)</source>
        <translation>Version %1  (%2)</translation>
    </message>
</context>
<context>
    <name>GestionnaireTLE</name>
    <message>
        <location filename="gestionnairetle.ui" line="35"/>
        <source>Gestionnaire de téléchargement des TLE</source>
        <translation>Management of TLE downloading</translation>
    </message>
    <message>
        <location filename="gestionnairetle.ui" line="55"/>
        <source>Groupe de TLE :</source>
        <translation>TLE group :</translation>
    </message>
    <message>
        <location filename="gestionnairetle.ui" line="90"/>
        <source>Fichiers TLE :</source>
        <translation>TLE files :</translation>
    </message>
    <message>
        <location filename="gestionnairetle.ui" line="106"/>
        <source>Mise à jour automatique du groupe</source>
        <translation>Automatic update of the group</translation>
    </message>
    <message>
        <location filename="gestionnairetle.ui" line="140"/>
        <source>Domaine :</source>
        <translation>Domain :</translation>
    </message>
    <message>
        <location filename="gestionnairetle.ui" line="182"/>
        <source>Nom du groupe :</source>
        <translation>Name of group :</translation>
    </message>
    <message>
        <location filename="gestionnairetle.ui" line="198"/>
        <source>Liste des fichiers TLE :</source>
        <translation>List of TLE files :</translation>
    </message>
    <message>
        <location filename="gestionnairetle.ui" line="214"/>
        <source>Valider</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="gestionnairetle.ui" line="230"/>
        <source>Annuler</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="gestionnairetle.ui" line="260"/>
        <source>Fermer</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="gestionnairetle.ui" line="298"/>
        <source>Age maximal des TLE (en jours) :</source>
        <translation>TLE expiry date (in days) :</translation>
    </message>
    <message>
        <location filename="gestionnairetle.ui" line="395"/>
        <source>MenuContextuelFichiersTLE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gestionnairetle.ui" line="405"/>
        <source>Créer un groupe</source>
        <translation>Create a group</translation>
    </message>
    <message>
        <location filename="gestionnairetle.ui" line="410"/>
        <location filename="gestionnairetle.ui" line="420"/>
        <source>Supprimer</source>
        <translation>Remove</translation>
    </message>
    <message>
        <location filename="gestionnairetle.ui" line="415"/>
        <source>Ajouter des fichiers</source>
        <translation>Add files</translation>
    </message>
    <message>
        <location filename="gestionnairetle.cpp" line="133"/>
        <source>Créer un groupe de TLE</source>
        <translation>Create a TLE group</translation>
    </message>
    <message>
        <location filename="gestionnairetle.cpp" line="137"/>
        <source>Ajouter des fichiers TLE</source>
        <translation>Add TLE files</translation>
    </message>
    <message>
        <location filename="gestionnairetle.cpp" line="245"/>
        <source>Voulez-vous vraiment supprimer le groupe &quot;%1&quot;?</source>
        <translation>Do you really want to remove the &quot;%1&quot; group?</translation>
    </message>
    <message>
        <location filename="gestionnairetle.cpp" line="246"/>
        <location filename="gestionnairetle.cpp" line="456"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="gestionnairetle.cpp" line="248"/>
        <location filename="gestionnairetle.cpp" line="458"/>
        <source>Oui</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="gestionnairetle.cpp" line="249"/>
        <location filename="gestionnairetle.cpp" line="459"/>
        <source>Non</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="gestionnairetle.cpp" line="354"/>
        <source>Le nom du domaine n&apos;est pas spécifié</source>
        <translation>The name of the domain is not given</translation>
    </message>
    <message>
        <location filename="gestionnairetle.cpp" line="357"/>
        <source>Le nom du groupe n&apos;est pas spécifié</source>
        <translation>The name of the group is not given</translation>
    </message>
    <message>
        <location filename="gestionnairetle.cpp" line="454"/>
        <source>Voulez-vous vraiment supprimer ce(s) fichier(s) du groupe &quot;%1&quot;?</source>
        <translation>Do you really want to remove this(these) file(s) from the &quot;%1&quot; group?</translation>
    </message>
</context>
<context>
    <name>PreviSat</name>
    <message>
        <location filename="previsat.ui" line="2220"/>
        <location filename="previsat.cpp" line="5355"/>
        <source>Mode manuel</source>
        <translation>Manual mode</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2145"/>
        <location filename="previsat.ui" line="2240"/>
        <location filename="previsat.cpp" line="6798"/>
        <location filename="previsat.cpp" line="6804"/>
        <source>secondes</source>
        <translation>seconds</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2305"/>
        <source>Fichier :</source>
        <translation>File :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2671"/>
        <source>Général</source>
        <translation>Main</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2727"/>
        <location filename="previsat.ui" line="8426"/>
        <location filename="previsat.ui" line="8991"/>
        <location filename="previsat.ui" line="13438"/>
        <location filename="previsat.ui" line="14222"/>
        <location filename="previsat.cpp" line="4919"/>
        <source>Lieu d&apos;observation :</source>
        <translation>Name of location :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2900"/>
        <location filename="previsat.ui" line="4729"/>
        <location filename="previsat.ui" line="7436"/>
        <location filename="previsat.ui" line="7550"/>
        <location filename="previsat.ui" line="8139"/>
        <location filename="previsat.ui" line="9569"/>
        <location filename="previsat.ui" line="10117"/>
        <source>Nom :</source>
        <translation>Name :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3040"/>
        <location filename="previsat.ui" line="4884"/>
        <location filename="previsat.ui" line="7094"/>
        <location filename="previsat.ui" line="7563"/>
        <source>ISS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2803"/>
        <location filename="previsat.ui" line="3059"/>
        <location filename="previsat.ui" line="9585"/>
        <location filename="previsat.ui" line="10005"/>
        <source>Longitude :</source>
        <translation>Longitude :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2784"/>
        <location filename="previsat.ui" line="3078"/>
        <source>000° 00&apos; 00&quot; Ouest</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2860"/>
        <location filename="previsat.ui" line="3097"/>
        <location filename="previsat.ui" line="9662"/>
        <location filename="previsat.ui" line="10053"/>
        <source>Latitude :</source>
        <translation>Latitude :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2841"/>
        <location filename="previsat.ui" line="3116"/>
        <source>00° 00&apos; 00&quot; Nord</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2765"/>
        <location filename="previsat.ui" line="3135"/>
        <location filename="previsat.ui" line="9726"/>
        <location filename="previsat.ui" line="10069"/>
        <source>Altitude :</source>
        <translation>Altitude :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3154"/>
        <location filename="previsat.ui" line="3274"/>
        <source>00000.0 km</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3173"/>
        <location filename="previsat.ui" line="3793"/>
        <location filename="previsat.ui" line="4027"/>
        <source>Hauteur :</source>
        <translation>Elevation :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3192"/>
        <location filename="previsat.ui" line="3350"/>
        <location filename="previsat.ui" line="3771"/>
        <location filename="previsat.ui" line="3888"/>
        <location filename="previsat.ui" line="4046"/>
        <location filename="previsat.ui" line="4068"/>
        <source>+00° 00&apos; 00&quot;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3214"/>
        <location filename="previsat.ui" line="3812"/>
        <location filename="previsat.ui" line="4169"/>
        <source>Azimut (N) :</source>
        <translation>Azimuth (N) :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3233"/>
        <location filename="previsat.ui" line="3749"/>
        <location filename="previsat.ui" line="4109"/>
        <source>000° 00&apos; 00&quot;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3255"/>
        <location filename="previsat.ui" line="3831"/>
        <location filename="previsat.ui" line="4207"/>
        <source>Distance :</source>
        <translation>Range :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3293"/>
        <location filename="previsat.ui" line="3910"/>
        <location filename="previsat.ui" line="4090"/>
        <location filename="previsat.cpp" line="6553"/>
        <source>Ascension droite :</source>
        <translation>Right ascension :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3312"/>
        <location filename="previsat.ui" line="3869"/>
        <location filename="previsat.ui" line="4150"/>
        <source>00h 00m 00s</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3372"/>
        <location filename="previsat.ui" line="3967"/>
        <location filename="previsat.ui" line="4226"/>
        <source>Constellation :</source>
        <translation>Constellation :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3391"/>
        <location filename="previsat.ui" line="3929"/>
        <location filename="previsat.ui" line="4008"/>
        <source>And</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3410"/>
        <source>Direction :</source>
        <translation>Direction :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3429"/>
        <location filename="previsat.cpp" line="2871"/>
        <source>Descendant</source>
        <translation>Descending</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3451"/>
        <source>Vitesse orbitale :</source>
        <translation>Orbital velocity :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3470"/>
        <source>7.777 km/s</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3492"/>
        <source>Variation distance :</source>
        <translation>Range rate :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3511"/>
        <source>+7.777 km/s</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3533"/>
        <source>Magnitude (Illumination) : -2.2 (50%)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3552"/>
        <source>Orbite n°</source>
        <translation>Orbit #</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3571"/>
        <source>99999</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="4630"/>
        <location filename="previsat.cpp" line="2781"/>
        <source>Crépuscule astronomique</source>
        <translation>Astronomical twilight</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3590"/>
        <source>Temps écoulé depuis l&apos;époque :</source>
        <translation>Time elapsed since epoch :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3609"/>
        <location filename="previsat.ui" line="13782"/>
        <source>-360.3 jours</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2689"/>
        <location filename="previsat.ui" line="4695"/>
        <location filename="previsat.cpp" line="4774"/>
        <location filename="previsat.cpp" line="4917"/>
        <source>Date :</source>
        <translation>Date :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2822"/>
        <source>0000 m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3730"/>
        <location filename="previsat.cpp" line="4956"/>
        <source>Coordonnées du Soleil :</source>
        <translation>Sun coordinates :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3850"/>
        <source>1.017 UA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3989"/>
        <location filename="previsat.cpp" line="4967"/>
        <source>Coordonnées de la Lune :</source>
        <translation>Moon coordinates :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="4188"/>
        <source>384400 km</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="4245"/>
        <source>Phase :</source>
        <translation>Phase :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="4264"/>
        <source>Gibbeuse décroissante</source>
        <translation>Waning gibbous</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="4283"/>
        <location filename="previsat.cpp" line="4977"/>
        <source>Illumination :</source>
        <translation>Illumination :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="4302"/>
        <source>100%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="4664"/>
        <location filename="previsat.ui" line="5233"/>
        <location filename="previsat.cpp" line="2745"/>
        <source>Éléments osculateurs</source>
        <translation>Osculating elements</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="4713"/>
        <source>ligne1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="4747"/>
        <source>ligne2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5010"/>
        <location filename="previsat.cpp" line="4781"/>
        <source>Vecteur d&apos;état</source>
        <translation>State vector</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5200"/>
        <source>x :</source>
        <translation>x :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5025"/>
        <source>y :</source>
        <translation>y :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5149"/>
        <source>z :</source>
        <translation>z :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5041"/>
        <location filename="previsat.ui" line="5076"/>
        <location filename="previsat.ui" line="5216"/>
        <source>+36000.000 km</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5095"/>
        <location filename="previsat.ui" line="5114"/>
        <location filename="previsat.ui" line="5165"/>
        <source>+29.000000 km/s</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5060"/>
        <source>vz :</source>
        <translation>vz :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5184"/>
        <source>vy :</source>
        <translation>vy :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5133"/>
        <source>vx :</source>
        <translation>vx :</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4795"/>
        <source>Éléments osculateurs :</source>
        <translation>Osculating elements :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6040"/>
        <source>Demi-grand axe :</source>
        <translation>Semi-major axis :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6021"/>
        <source>36000.0 km</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5527"/>
        <location filename="previsat.ui" line="5791"/>
        <location filename="previsat.ui" line="6791"/>
        <location filename="previsat.ui" line="12264"/>
        <source>Excentricité :</source>
        <translation>Eccentricity :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5317"/>
        <location filename="previsat.ui" line="5384"/>
        <location filename="previsat.ui" line="5403"/>
        <location filename="previsat.ui" line="5422"/>
        <location filename="previsat.ui" line="5511"/>
        <location filename="previsat.ui" line="5594"/>
        <location filename="previsat.ui" line="5613"/>
        <location filename="previsat.ui" line="5772"/>
        <location filename="previsat.ui" line="5931"/>
        <location filename="previsat.ui" line="5982"/>
        <location filename="previsat.ui" line="6750"/>
        <location filename="previsat.ui" line="12359"/>
        <source>0.0000000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5667"/>
        <location filename="previsat.ui" line="5966"/>
        <location filename="previsat.ui" line="6536"/>
        <location filename="previsat.ui" line="7754"/>
        <location filename="previsat.ui" line="12395"/>
        <source>Inclinaison :</source>
        <translation>Inclination :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5298"/>
        <location filename="previsat.ui" line="5492"/>
        <location filename="previsat.ui" line="5575"/>
        <location filename="previsat.ui" line="5683"/>
        <location filename="previsat.ui" line="5702"/>
        <location filename="previsat.ui" line="5737"/>
        <location filename="previsat.ui" line="5807"/>
        <location filename="previsat.ui" line="5877"/>
        <location filename="previsat.ui" line="5912"/>
        <location filename="previsat.ui" line="6001"/>
        <location filename="previsat.ui" line="6056"/>
        <location filename="previsat.ui" line="6075"/>
        <location filename="previsat.ui" line="6161"/>
        <location filename="previsat.ui" line="6853"/>
        <location filename="previsat.ui" line="6875"/>
        <location filename="previsat.ui" line="7132"/>
        <location filename="previsat.ui" line="7205"/>
        <location filename="previsat.ui" line="7773"/>
        <source>000.0000°</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6234"/>
        <source>Anomalie vraie :</source>
        <translation>True anomaly :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6250"/>
        <source>Anomalie excentrique :</source>
        <translation>Eccentric anomaly :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6199"/>
        <source>Champ de vue :</source>
        <translation>Field of view :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5651"/>
        <location filename="previsat.ui" line="5845"/>
        <location filename="previsat.ui" line="7417"/>
        <location filename="previsat.ui" line="12095"/>
        <source>AD noeud ascendant :</source>
        <translation>RA of ascending node :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5756"/>
        <location filename="previsat.ui" line="7113"/>
        <location filename="previsat.ui" line="12806"/>
        <source>Argument du périgée :</source>
        <translation>Argument of perigee :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5476"/>
        <location filename="previsat.ui" line="5721"/>
        <location filename="previsat.ui" line="6555"/>
        <source>Anomalie moyenne :</source>
        <translation>Mean anomaly :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6094"/>
        <location filename="previsat.ui" line="7954"/>
        <source>Apogée (Altitude) :</source>
        <translation>Apogee (Altitude) :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6180"/>
        <location filename="previsat.ui" line="7989"/>
        <source>36000.0 km (36000.0 km)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6110"/>
        <location filename="previsat.ui" line="7938"/>
        <source>Périgée (Altitude) :</source>
        <translation>Perigee (Altitude) :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6126"/>
        <location filename="previsat.ui" line="7903"/>
        <source>Période orbitale :</source>
        <translation>Orbital period :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6142"/>
        <location filename="previsat.ui" line="7919"/>
        <source>24h 50m 53s</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6370"/>
        <location filename="previsat.cpp" line="2746"/>
        <location filename="previsat.cpp" line="8255"/>
        <source>Informations satellite</source>
        <translation>Satellite informations</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6593"/>
        <location filename="previsat.ui" line="7620"/>
        <location filename="previsat.ui" line="11958"/>
        <source>Numéro NORAD :</source>
        <translation>NORAD number :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="7398"/>
        <location filename="previsat.ui" line="7601"/>
        <source>Désignation COSPAR :</source>
        <translation>COSPAR Designation :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="7227"/>
        <source>Époque (UTC) :</source>
        <translation>Epoch (UTC) :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6653"/>
        <source>Coeff pseudo-balistique :</source>
        <translation>Pseudo-ballistic coef :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="7284"/>
        <source>+5.76279999</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6463"/>
        <source>(1/Re)</source>
        <translation>(1/Er)</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="7379"/>
        <source>2011/07/31 00:00:00</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6612"/>
        <location filename="previsat.ui" line="7582"/>
        <source>1998-067A</source>
        <translation>1998-067A</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6425"/>
        <location filename="previsat.ui" line="6517"/>
        <location filename="previsat.ui" line="7639"/>
        <source>25544</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6810"/>
        <source>Moyen mouvement :</source>
        <translation>Mean motion :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6631"/>
        <location filename="previsat.ui" line="6935"/>
        <location filename="previsat.ui" line="7262"/>
        <source>15.76279999</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6897"/>
        <location filename="previsat.ui" line="6916"/>
        <location filename="previsat.ui" line="7360"/>
        <source>rev/jour</source>
        <translation>rev/day</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="7303"/>
        <source>n&apos; / 2 :</source>
        <translation>n&apos; / 2 :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6712"/>
        <location filename="previsat.ui" line="7170"/>
        <location filename="previsat.ui" line="8011"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6574"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6444"/>
        <source>n&quot; / 6 :</source>
        <translation>n&quot; / 6 :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="7341"/>
        <source>Nb orbites à l&apos;époque :</source>
        <translation>Orbit # at epoch :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="7455"/>
        <location filename="previsat.ui" line="7868"/>
        <source>Magnitude std/max :</source>
        <translation>Std/Max magnitude :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="7322"/>
        <location filename="previsat.ui" line="7849"/>
        <source>-0.5v/-2.9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6731"/>
        <location filename="previsat.ui" line="7792"/>
        <source>Modèle orbital :</source>
        <translation>Propagation model :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6772"/>
        <location filename="previsat.ui" line="7811"/>
        <location filename="previsat.cpp" line="3174"/>
        <location filename="previsat.cpp" line="8453"/>
        <source>SGP4 (NE)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6498"/>
        <location filename="previsat.ui" line="7887"/>
        <source>Dimensions/Section :</source>
        <translation>Dimensions/Section :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="7474"/>
        <location filename="previsat.ui" line="7830"/>
        <source>Cylindrique. L=30.0m, R=20.0m / 350.00 m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8245"/>
        <source>Prévisions</source>
        <translation>Predictions</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8263"/>
        <location filename="previsat.ui" line="8837"/>
        <location filename="previsat.ui" line="13097"/>
        <location filename="previsat.ui" line="13393"/>
        <location filename="previsat.ui" line="14206"/>
        <source>Date initiale :</source>
        <translation>Start date :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8307"/>
        <location filename="previsat.ui" line="8888"/>
        <location filename="previsat.ui" line="13065"/>
        <location filename="previsat.ui" line="13409"/>
        <location filename="previsat.ui" line="14266"/>
        <source>Date finale :</source>
        <translation>Finish date :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8822"/>
        <source>Flashs Iridium</source>
        <translation>Iridium flares</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9486"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9523"/>
        <location filename="previsat.cpp" line="9753"/>
        <source>Lieu d&apos;observation</source>
        <translation>Location</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9892"/>
        <source>Sélection de la catégorie :</source>
        <translation>Category selection :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9538"/>
        <source>Lieux d&apos;observation :</source>
        <translation>Locations in the category :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10179"/>
        <source>Lieux sélectionnés :</source>
        <translation>Selected locations :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="720"/>
        <location filename="previsat.ui" line="9640"/>
        <location filename="previsat.ui" line="14349"/>
        <location filename="previsat.cpp" line="2612"/>
        <location filename="previsat.cpp" line="2617"/>
        <location filename="previsat.cpp" line="2836"/>
        <location filename="previsat.cpp" line="3579"/>
        <location filename="previsat.cpp" line="4433"/>
        <location filename="previsat.cpp" line="6356"/>
        <location filename="previsat.cpp" line="9700"/>
        <source>Est</source>
        <translation>East</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2150"/>
        <location filename="previsat.cpp" line="6805"/>
        <source>minutes</source>
        <translation>minutes</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2155"/>
        <location filename="previsat.cpp" line="6806"/>
        <source>heures</source>
        <translation>hours</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2160"/>
        <location filename="previsat.cpp" line="6807"/>
        <source>jours</source>
        <translation>days</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2260"/>
        <location filename="previsat.ui" line="2325"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2265"/>
        <location filename="previsat.ui" line="2330"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2270"/>
        <location filename="previsat.ui" line="2335"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2275"/>
        <location filename="previsat.ui" line="2340"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2280"/>
        <location filename="previsat.ui" line="2345"/>
        <source>30</source>
        <translation>30</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2285"/>
        <location filename="previsat.ui" line="2350"/>
        <source>60</source>
        <translation>60</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="701"/>
        <location filename="previsat.ui" line="9645"/>
        <location filename="previsat.ui" line="14327"/>
        <location filename="previsat.cpp" line="2613"/>
        <location filename="previsat.cpp" line="2616"/>
        <location filename="previsat.cpp" line="2836"/>
        <location filename="previsat.cpp" line="3579"/>
        <location filename="previsat.cpp" line="4433"/>
        <location filename="previsat.cpp" line="6356"/>
        <source>Ouest</source>
        <translation>West</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="657"/>
        <location filename="previsat.ui" line="9704"/>
        <location filename="previsat.ui" line="14368"/>
        <location filename="previsat.cpp" line="2604"/>
        <location filename="previsat.cpp" line="2607"/>
        <location filename="previsat.cpp" line="2839"/>
        <location filename="previsat.cpp" line="3580"/>
        <location filename="previsat.cpp" line="4434"/>
        <location filename="previsat.cpp" line="6360"/>
        <source>Nord</source>
        <translation>North</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="679"/>
        <location filename="previsat.ui" line="9709"/>
        <location filename="previsat.ui" line="14390"/>
        <location filename="previsat.cpp" line="2603"/>
        <location filename="previsat.cpp" line="2608"/>
        <location filename="previsat.cpp" line="2839"/>
        <location filename="previsat.cpp" line="3580"/>
        <location filename="previsat.cpp" line="4434"/>
        <location filename="previsat.cpp" line="6360"/>
        <location filename="previsat.cpp" line="9701"/>
        <source>Sud</source>
        <translation>South</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9764"/>
        <location filename="previsat.cpp" line="3182"/>
        <location filename="previsat.cpp" line="3587"/>
        <location filename="previsat.cpp" line="4441"/>
        <location filename="previsat.cpp" line="8461"/>
        <location filename="previsat.cpp" line="9502"/>
        <location filename="previsat.cpp" line="9606"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9780"/>
        <source>Ajouter dans :</source>
        <translation>Add in :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9809"/>
        <location filename="previsat.ui" line="10146"/>
        <source>Valider</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9825"/>
        <location filename="previsat.ui" line="10162"/>
        <source>Annuler</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="167"/>
        <location filename="previsat.cpp" line="6741"/>
        <source>Agrandir</source>
        <translation>Maximize</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="429"/>
        <source>W</source>
        <translation>W</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="448"/>
        <source>E</source>
        <translation>E</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="584"/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="603"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="748"/>
        <location filename="previsat.cpp" line="6772"/>
        <source>Carte du ciel</source>
        <translation>Sky map</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2180"/>
        <location filename="previsat.cpp" line="889"/>
        <source>Mode de fonctionnement</source>
        <translation>Mode</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2708"/>
        <location filename="previsat.ui" line="4679"/>
        <source>Mercredi 31 décembre 2022  00:00:00                UTC + 01:00</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="4374"/>
        <location filename="previsat.ui" line="4910"/>
        <location filename="previsat.cpp" line="4549"/>
        <location filename="previsat.cpp" line="5428"/>
        <location filename="previsat.cpp" line="6025"/>
        <location filename="previsat.cpp" line="8054"/>
        <location filename="previsat.cpp" line="9946"/>
        <source>dddd dd MMMM yyyy  hh:mm:ss</source>
        <translation>dddd, MMMM dd yyyy  hh:mm:ss</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2198"/>
        <location filename="previsat.cpp" line="5345"/>
        <source>Temps réel</source>
        <translation>Real time</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="23"/>
        <source>PreviSat</source>
        <translation>PreviSat</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="924"/>
        <source>GMT = 000/00:00</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="1426"/>
        <source>LAT = 45.6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="1485"/>
        <source>ALT = 225.5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="1544"/>
        <source>LON = -101.3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="1603"/>
        <source>INC = 51.6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="1662"/>
        <source>D/N : 0:17:36</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="1721"/>
        <source>ORB = 86861</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="1780"/>
        <source>BETA = 75.3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2537"/>
        <source>Météo bases NASA</source>
        <translation>Weather of NASA bases</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2540"/>
        <source>NASA</source>
        <translation>NASA</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3331"/>
        <location filename="previsat.ui" line="3948"/>
        <location filename="previsat.ui" line="4131"/>
        <location filename="previsat.cpp" line="6555"/>
        <source>Déclinaison :</source>
        <translation>Declination :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3625"/>
        <source>Prochain AOS :</source>
        <translation>Next AOS :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="3644"/>
        <source>2011/07/31 00:00:00 (dans 00min 00s). Azimut : 000° 00&apos;</source>
        <translation>2011/07/31 00:00:00 (in 00min 00s). Azimuth : 000° 00&apos;</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="4321"/>
        <source>Magnitude :</source>
        <translation>Magnitude :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="4340"/>
        <source>-12.73</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="4396"/>
        <location filename="previsat.ui" line="4929"/>
        <source>UTC + 01:00</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="4649"/>
        <source>Conditions :</source>
        <translation>Conditions :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6215"/>
        <location filename="previsat.ui" line="7970"/>
        <source>036000.0 km (036000.0 km)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="4952"/>
        <source>Paramètres képlériens</source>
        <translation>Keplerian parameters</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="4957"/>
        <source>Paramètres circulaires</source>
        <translation>Circular parameters</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="4962"/>
        <source>Paramètres équatoriaux</source>
        <translation>Equatorial parameters</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="4967"/>
        <source>Paramètres circulaires équatoriaux</source>
        <translation>Circular equatorial parameters</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5368"/>
        <location filename="previsat.ui" line="5861"/>
        <source>Ey :</source>
        <translation>Ey :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5896"/>
        <source>Position sur orbite :</source>
        <translation>Position on orbit :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5336"/>
        <location filename="previsat.ui" line="5950"/>
        <source>Ex :</source>
        <translation>Ex :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5266"/>
        <location filename="previsat.ui" line="5460"/>
        <source>Ix :</source>
        <translation>Ix :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5352"/>
        <location filename="previsat.ui" line="5543"/>
        <source>Iy :</source>
        <translation>Iy :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5559"/>
        <source>Longitude du périgée :</source>
        <translation>Longitude of perigee :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="5282"/>
        <source>Argument longitude vraie :</source>
        <translation>True longitude argument :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="4991"/>
        <source>ECI</source>
        <translation>ECI</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="4996"/>
        <source>ECEF</source>
        <translation>ECEF</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6264"/>
        <source>Signal</source>
        <translation>Signal</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6276"/>
        <source>Doppler@100MHz :</source>
        <translation>Doppler@100MHz :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6289"/>
        <source>Atténuation :</source>
        <translation>Free-space loss :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6302"/>
        <source>1824 Hz</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6315"/>
        <source>27.57 ms</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6328"/>
        <source>Délai :</source>
        <translation>Delay :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6341"/>
        <source>150.74 dB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="7243"/>
        <location filename="previsat.ui" line="7719"/>
        <source>Date de lancement :</source>
        <translation>Launch date :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6669"/>
        <location filename="previsat.ui" line="7671"/>
        <location filename="previsat.ui" line="8027"/>
        <source>2013/07/08</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6954"/>
        <location filename="previsat.ui" line="7735"/>
        <source>Catégorie d&apos;orbite :</source>
        <translation>Orbital category :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6479"/>
        <location filename="previsat.ui" line="7655"/>
        <source>LEO/I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="7186"/>
        <location filename="previsat.ui" line="7687"/>
        <source>US</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="7151"/>
        <location filename="previsat.ui" line="7703"/>
        <source>Pays/Organisation :</source>
        <translation>Country/Organization :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8285"/>
        <location filename="previsat.ui" line="8329"/>
        <location filename="previsat.cpp" line="9940"/>
        <location filename="previsat.cpp" line="10022"/>
        <source>dd/MM/yyyy hh:mm:ss</source>
        <translation>MM/dd/yyyy hh:mm:ss</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8351"/>
        <source>Pas de génération :</source>
        <translation>Output step :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8371"/>
        <source>1 seconde</source>
        <translation>1 second</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8376"/>
        <source>5 secondes</source>
        <translation>5 seconds</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8381"/>
        <source>10 secondes</source>
        <translation>10 seconds</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8386"/>
        <source>20 secondes</source>
        <translation>20 seconds</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8391"/>
        <source>30 secondes</source>
        <translation>30 seconds</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8396"/>
        <source>1 minute</source>
        <translation>1 minute</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8401"/>
        <source>2 minutes</source>
        <translation>2 minutes</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8406"/>
        <source>5 minutes</source>
        <translation>5 minutes</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8461"/>
        <location filename="previsat.ui" line="8975"/>
        <location filename="previsat.ui" line="14047"/>
        <source>Hauteur du Soleil :</source>
        <translation>Sun elevation :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8490"/>
        <location filename="previsat.ui" line="8933"/>
        <location filename="previsat.ui" line="13983"/>
        <source>Horizon (0°)</source>
        <translation>Horizon (0°)</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8495"/>
        <location filename="previsat.ui" line="8938"/>
        <location filename="previsat.ui" line="13988"/>
        <source>Crépuscule civil (-6°)</source>
        <translation>Civil twilight (-6°)</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8500"/>
        <location filename="previsat.ui" line="8943"/>
        <location filename="previsat.ui" line="13993"/>
        <source>Crépuscule nautique (-12°)</source>
        <translation>Nautical twilight (-12°)</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8505"/>
        <location filename="previsat.ui" line="8948"/>
        <location filename="previsat.ui" line="13998"/>
        <source>Crépuscule astronomique (-18°)</source>
        <translation>Astronomical twilight (-18°)</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8510"/>
        <location filename="previsat.ui" line="8953"/>
        <location filename="previsat.ui" line="14003"/>
        <source>Indifférent</source>
        <translation>Indifferent</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8515"/>
        <location filename="previsat.ui" line="8580"/>
        <location filename="previsat.ui" line="8958"/>
        <location filename="previsat.ui" line="9065"/>
        <location filename="previsat.ui" line="12117"/>
        <location filename="previsat.ui" line="12286"/>
        <location filename="previsat.ui" line="13015"/>
        <location filename="previsat.ui" line="13540"/>
        <location filename="previsat.ui" line="13940"/>
        <location filename="previsat.ui" line="14008"/>
        <source>Autre...</source>
        <translation>Other...</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8535"/>
        <location filename="previsat.ui" line="9020"/>
        <location filename="previsat.ui" line="13579"/>
        <location filename="previsat.ui" line="14133"/>
        <source>Hauteur minimale du satellite :</source>
        <translation>Minimal elevation of satellite :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8555"/>
        <location filename="previsat.ui" line="9040"/>
        <location filename="previsat.ui" line="13515"/>
        <location filename="previsat.ui" line="13915"/>
        <source>0°</source>
        <translation>0°</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8560"/>
        <location filename="previsat.ui" line="9045"/>
        <location filename="previsat.ui" line="13520"/>
        <location filename="previsat.ui" line="13920"/>
        <source>5°</source>
        <translation>5°</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8565"/>
        <location filename="previsat.ui" line="9050"/>
        <location filename="previsat.ui" line="13525"/>
        <location filename="previsat.ui" line="13925"/>
        <source>10°</source>
        <translation>10°</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8570"/>
        <location filename="previsat.ui" line="9055"/>
        <location filename="previsat.ui" line="13530"/>
        <location filename="previsat.ui" line="13930"/>
        <source>15°</source>
        <translation>15°</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8575"/>
        <location filename="previsat.ui" line="9060"/>
        <location filename="previsat.ui" line="13535"/>
        <location filename="previsat.ui" line="13935"/>
        <source>20°</source>
        <translation>20°</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8600"/>
        <source>Illumination requise</source>
        <translation>Illumination required</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8622"/>
        <source>Magnitude maximale</source>
        <translation>Maximal magnitude</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8700"/>
        <location filename="previsat.ui" line="8872"/>
        <location filename="previsat.ui" line="13081"/>
        <location filename="previsat.ui" line="13377"/>
        <location filename="previsat.ui" line="13879"/>
        <source>Effacer heures</source>
        <translation>Erase hours</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8719"/>
        <location filename="previsat.ui" line="9338"/>
        <location filename="previsat.ui" line="13274"/>
        <location filename="previsat.ui" line="13709"/>
        <location filename="previsat.ui" line="13841"/>
        <source>Calculs</source>
        <translation>Run</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8741"/>
        <location filename="previsat.ui" line="9322"/>
        <location filename="previsat.ui" line="13293"/>
        <location filename="previsat.ui" line="13693"/>
        <location filename="previsat.ui" line="13895"/>
        <source>Afficher</source>
        <translation>Show results</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8807"/>
        <location filename="previsat.ui" line="9401"/>
        <location filename="previsat.ui" line="13309"/>
        <location filename="previsat.ui" line="13728"/>
        <location filename="previsat.ui" line="14117"/>
        <source>Paramétrage par défaut</source>
        <translation>Default settings</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9082"/>
        <source>Uniquement les satellites opérationnels</source>
        <translation>Only operational satellites</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9101"/>
        <location filename="previsat.ui" line="13860"/>
        <source>Classer par ordre chronologique</source>
        <translation>Chronologically sorting</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9120"/>
        <location filename="previsat.ui" line="13454"/>
        <location filename="previsat.ui" line="14282"/>
        <source>Fichier TLE :</source>
        <translation>TLE file :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9446"/>
        <source>Satellite</source>
        <translation>Satellite</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9451"/>
        <source>Statut</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9468"/>
        <source>Inclure les flashs des panneaux solaires</source>
        <translation>Include solar panels flares</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10238"/>
        <source>Configuration</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10316"/>
        <source>Système</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10573"/>
        <source>Affichage</source>
        <translation>Display</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10785"/>
        <source>Vision nocturne :</source>
        <translation>Night vision :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10559"/>
        <source>Vérification des mises à jour au démarrage</source>
        <translation>Check for updates at startup</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10690"/>
        <source>Garder les proportions de la carte du monde</source>
        <translation>Preserve proportions of world map</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10817"/>
        <source>Prise en compte de la réfraction pour les éclipses</source>
        <translation>Set the atmospheric refraction for eclipses</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10505"/>
        <source>Système horaire</source>
        <translation>Time convention</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="6382"/>
        <location filename="previsat.cpp" line="8260"/>
        <source>Recherche données satellite...</source>
        <translation>Satellite data search...</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8043"/>
        <source>Date de rentrée :</source>
        <translation>Decay date :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="7506"/>
        <location filename="previsat.ui" line="8059"/>
        <source>AFETR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="7490"/>
        <location filename="previsat.ui" line="8075"/>
        <source>Site de lancement :</source>
        <translation>Launch site :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8089"/>
        <location filename="previsat.cpp" line="8602"/>
        <source>Objets trouvés :</source>
        <translation>Found objects :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8120"/>
        <source>Entrez au minimum 3 lettres du nom de l&apos;objet</source>
        <translation>Enter at least 3 letters of the object name</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8152"/>
        <source>NORAD :</source>
        <translation>NORAD :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8184"/>
        <source>COSPAR :</source>
        <translation>COSPAR :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8760"/>
        <location filename="previsat.ui" line="9357"/>
        <location filename="previsat.ui" line="14025"/>
        <source>###</source>
        <translation>###</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8785"/>
        <location filename="previsat.ui" line="9379"/>
        <location filename="previsat.ui" line="13557"/>
        <location filename="previsat.ui" line="14168"/>
        <source>99</source>
        <translation>99</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10520"/>
        <source>24 heures</source>
        <translation>24 hours</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10539"/>
        <source>12 heures (AM/PM)</source>
        <translation>12 hours (AM/PM)</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10884"/>
        <source>Icône des satellites</source>
        <translation>Satellite icons</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11254"/>
        <source>Affichage de l&apos;angle beta</source>
        <translation>Display of beta angle</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11270"/>
        <source>Style &quot;Wall Command Center&quot;</source>
        <translation>&quot;Wall Command Center&quot; style</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11305"/>
        <source>Stations :</source>
        <translation>Stations :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11321"/>
        <source>Affichage des cercles d&apos;acquisition</source>
        <translation>Display of acquisition circles</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11343"/>
        <source>Affichage de la SAA et de la ZOE</source>
        <translation>Display of SAA and ZOE</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11362"/>
        <source>Couleur de GMT :</source>
        <translation>Color of GMT :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11376"/>
        <location filename="previsat.ui" line="11493"/>
        <location filename="previsat.ui" line="11564"/>
        <source>Rouge</source>
        <translation>Red</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11381"/>
        <location filename="previsat.ui" line="11406"/>
        <location filename="previsat.ui" line="11498"/>
        <location filename="previsat.ui" line="11559"/>
        <source>Blanc</source>
        <translation>White</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11386"/>
        <source>Cyan</source>
        <translation>Cyan</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11401"/>
        <source>Noir</source>
        <translation>Black</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11420"/>
        <source>Couleur de la ZOE :</source>
        <translation>Color of ZOE :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11433"/>
        <source>Affichage du nombre d&apos;orbites</source>
        <translation>Display of orbit number</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11446"/>
        <source>Choix de la police :</source>
        <translation>Set the font :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11460"/>
        <source>Lucida Console</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11465"/>
        <source>MS Shell Dlg 2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11479"/>
        <source>Couleur de l&apos;équateur :</source>
        <translation>Color of equator :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11518"/>
        <source>Jaune</source>
        <translation>Yellow</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11513"/>
        <source>Brun</source>
        <translation>Brown</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11532"/>
        <source>Couleur du terminateur :</source>
        <translation>Color of terminator :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11545"/>
        <source>Couleur du cercle de visibilité :</source>
        <translation>Color of ISS footprint :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11792"/>
        <location filename="previsat.ui" line="11882"/>
        <location filename="previsat.ui" line="11942"/>
        <location filename="previsat.ui" line="12704"/>
        <location filename="previsat.cpp" line="540"/>
        <location filename="previsat.cpp" line="555"/>
        <location filename="previsat.cpp" line="570"/>
        <location filename="previsat.cpp" line="1447"/>
        <location filename="previsat.cpp" line="4389"/>
        <location filename="previsat.cpp" line="4662"/>
        <location filename="previsat.cpp" line="7617"/>
        <location filename="previsat.cpp" line="10933"/>
        <location filename="previsat.cpp" line="11056"/>
        <location filename="previsat.cpp" line="11481"/>
        <location filename="previsat.cpp" line="11586"/>
        <location filename="previsat.cpp" line="11750"/>
        <location filename="previsat.cpp" line="11869"/>
        <source>Parcourir...</source>
        <translation>Browse...</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="12356"/>
        <location filename="previsat.ui" line="12375"/>
        <source>9.9999999</source>
        <translation>9.9999999</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="12752"/>
        <location filename="previsat.ui" line="12787"/>
        <source>99.999999999</source>
        <translation>99.999999999</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="13763"/>
        <source>Age du TLE : </source>
        <translation>TLE age :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="13794"/>
        <source>Flashs MetOp et SkyMed</source>
        <translation>MetOp and SkyMed flares</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14540"/>
        <source>Chaîne :</source>
        <translation>Channel :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14662"/>
        <source>Cliquer pour activer
le flux vidéo</source>
        <translation>Click here to activate
the video stream</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="15010"/>
        <source>Mettre à jour les fichiers internes</source>
        <translation>Update internal files</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="15024"/>
        <location filename="previsat.ui" line="15027"/>
        <source>Fenêtre séparée</source>
        <translation>Separate window</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="15032"/>
        <location filename="previsat.ui" line="15035"/>
        <source>Fermer</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9139"/>
        <location filename="previsat.ui" line="14066"/>
        <source>Affichage des résultats</source>
        <translation>Output display</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9154"/>
        <location filename="previsat.ui" line="14081"/>
        <source>1 ligne</source>
        <translation>1 line</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9170"/>
        <location filename="previsat.ui" line="14097"/>
        <source>3 lignes</source>
        <translation>3 lines</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9190"/>
        <source>Magnitude maximale (nuit) :</source>
        <translation>Maximal magnitude (night) :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9262"/>
        <source>Magnitude maximale (jour) :</source>
        <translation>Maximal magnitude (day) :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9306"/>
        <source>Angle maximal de réflexion :</source>
        <translation>Maximal angle of reflexion :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9989"/>
        <location filename="previsat.cpp" line="4437"/>
        <source>Lieu :</source>
        <translation>Location :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10021"/>
        <source>179° 59&apos; 59&quot; Ouest</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10037"/>
        <source>79° 59&apos; 59&quot; Nord</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10085"/>
        <source>2000 m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11129"/>
        <location filename="previsat.ui" line="13673"/>
        <location filename="previsat.cpp" line="6419"/>
        <location filename="previsat.cpp" line="6512"/>
        <location filename="previsat.cpp" line="6516"/>
        <location filename="previsat.cpp" line="6640"/>
        <location filename="previsat.cpp" line="6644"/>
        <source>Lune</source>
        <translation>Moon</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11097"/>
        <source>Phase de la Lune</source>
        <translation>Moon phase</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11113"/>
        <source>Rotation de la Lune pour l&apos;hémisphère Sud</source>
        <translation>Moon rotation for southern hemisphere</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10865"/>
        <source>Nom des satellites</source>
        <translation>Name of satellites</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10900"/>
        <source>Zone de visibilité</source>
        <translation>Satellite footprint</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10919"/>
        <source>Trace au sol</source>
        <translation>Ground track</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10957"/>
        <source>Notification sonore</source>
        <translation>Sound notification</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10725"/>
        <source>Affichage du jour julien</source>
        <translation>Display of julian date</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10655"/>
        <source>Nom des lieux d&apos;observation</source>
        <translation>Name of locations</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10833"/>
        <source>Prise en compte de l&apos;extinction atmosphérique</source>
        <translation>Set the atmospheric extinction</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10639"/>
        <source>Affichage des coordonnées</source>
        <translation>Display of coordinates</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10674"/>
        <source>Carte du monde :</source>
        <translation>World map :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10337"/>
        <source>Unités</source>
        <translation>Units</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10352"/>
        <source>Système métrique</source>
        <translation>Metric system</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10371"/>
        <source>Système anglo-saxon</source>
        <translation>Anglo-Saxon system</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10391"/>
        <source>Écart Heure locale - UTC</source>
        <translation>Local hour - UTC offset</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10425"/>
        <source>Temps Universel Coordonné (UTC)</source>
        <translation>Universal Time Coordinated (UTC)</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="572"/>
        <source>Affichage des messages informatifs</source>
        <translation>Display of informative messages</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14862"/>
        <source>F1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14877"/>
        <location filename="previsat.ui" line="14880"/>
        <source>Télécharger la mise à jour</source>
        <translation>Download software update</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14944"/>
        <location filename="previsat.cpp" line="1229"/>
        <location filename="previsat.cpp" line="1240"/>
        <location filename="previsat.cpp" line="1363"/>
        <location filename="previsat.cpp" line="1379"/>
        <source>Télécharger...</source>
        <translation>Download...</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14973"/>
        <source>Imprimer carte</source>
        <translation>Print map</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14976"/>
        <source>Ctrl+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14981"/>
        <location filename="previsat.ui" line="14986"/>
        <source>Renommer</source>
        <translation>Rename</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14991"/>
        <source>Modifier coordonnées</source>
        <translation>Modify coordinates</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14999"/>
        <source>Vision nocturne</source>
        <translation>Night vision</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="15002"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10406"/>
        <source>Heure locale = </source>
        <translation>Local hour = </translation>
    </message>
    <message>
        <location filename="previsat.ui" line="9867"/>
        <location filename="previsat.ui" line="9933"/>
        <source>Cliquer droit pour afficher le menu contextuel</source>
        <translation>Right click to show the context menu</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10466"/>
        <source>UTC + 02:00</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10485"/>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11081"/>
        <source>Zone d&apos;ombre</source>
        <translation>Night shadow</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10623"/>
        <source>Inversion Nord/Sud sur le radar</source>
        <translation>North/South inversion on radar</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10591"/>
        <source>Grille</source>
        <translation>Grid</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10706"/>
        <source>Radar</source>
        <translation>Radar</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11192"/>
        <source>Nom des étoiles</source>
        <translation>Name of stars</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11227"/>
        <source>Magnitude limite des étoiles :</source>
        <translation>Limiting magnitude of stars :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11062"/>
        <location filename="previsat.ui" line="13654"/>
        <location filename="previsat.cpp" line="6400"/>
        <location filename="previsat.cpp" line="6493"/>
        <location filename="previsat.cpp" line="6497"/>
        <location filename="previsat.cpp" line="6619"/>
        <location filename="previsat.cpp" line="6623"/>
        <source>Soleil</source>
        <translation>Sun</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11208"/>
        <source>Affichage des constellations</source>
        <translation>Display of constellations</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10607"/>
        <source>Inversion Est/Ouest sur le radar</source>
        <translation>East/West inversion on radar</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10801"/>
        <source>Affichage de la SAA</source>
        <translation>Display of SAA</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11145"/>
        <source>Affichage des planètes</source>
        <translation>Display of planets</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11582"/>
        <source>Outils</source>
        <translation>Tools</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11616"/>
        <source>Mise à jour TLE</source>
        <translation>TLE update</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11837"/>
        <source>Fichier à mettre à jour :</source>
        <translation>TLE file to update :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11821"/>
        <location filename="previsat.ui" line="11913"/>
        <source>Fichier TLE à lire :</source>
        <translation>TLE File to read :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11853"/>
        <source>Mettre à jour</source>
        <translation>Update</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11723"/>
        <source>Paramétrage...</source>
        <translation>Settings...</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2518"/>
        <source>ISS Live</source>
        <translation>ISS Live</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2489"/>
        <source>Météo du lieu d&apos;observation</source>
        <translation>Weather of user location</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10847"/>
        <source>Satellites</source>
        <translation>Satellites</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="10973"/>
        <source>Trace dans le ciel</source>
        <translation>Sky track</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11005"/>
        <source>Rotation de l&apos;icône ISS</source>
        <translation>Rotate ISS icon</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11019"/>
        <source>Système solaire, étoiles</source>
        <translation>Solar system, stars</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11242"/>
        <source>Wall Command Center</source>
        <translation>Wall Command Center</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11777"/>
        <source>Mise à jour TLE manuelle</source>
        <translation>Manual TLE update</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11898"/>
        <source>Extraction fichier TLE</source>
        <translation>TLE file extraction</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11975"/>
        <location filename="previsat.ui" line="14924"/>
        <source>Tous</source>
        <translation>All</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11980"/>
        <source>Sélection...</source>
        <translation>Selection...</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="12018"/>
        <location filename="previsat.ui" line="12155"/>
        <location filename="previsat.ui" line="12324"/>
        <location filename="previsat.ui" line="12456"/>
        <location filename="previsat.ui" line="12736"/>
        <location filename="previsat.ui" line="12843"/>
        <source>de</source>
        <translation>from</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="12056"/>
        <location filename="previsat.ui" line="12193"/>
        <location filename="previsat.ui" line="12340"/>
        <location filename="previsat.ui" line="12526"/>
        <location filename="previsat.ui" line="12604"/>
        <location filename="previsat.ui" line="12771"/>
        <location filename="previsat.ui" line="12881"/>
        <source>à</source>
        <translation>to</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="12112"/>
        <location filename="previsat.ui" line="13010"/>
        <source>De 0° à 360°</source>
        <translation>From 0° to 360°</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="12231"/>
        <location filename="previsat.ui" line="12247"/>
        <location filename="previsat.ui" line="12472"/>
        <location filename="previsat.ui" line="12488"/>
        <location filename="previsat.ui" line="12642"/>
        <location filename="previsat.ui" line="12658"/>
        <location filename="previsat.ui" line="12919"/>
        <location filename="previsat.ui" line="12935"/>
        <source>°</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="12281"/>
        <source>De 0 à 1</source>
        <translation>From 0 to 1</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="12378"/>
        <source>1.0000000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="12412"/>
        <source>1 intervalle</source>
        <translation>1 interval</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="12417"/>
        <source>2 intervalles</source>
        <translation>2 intervals</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="12563"/>
        <source>et de</source>
        <translation>and from</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="12675"/>
        <source>Nom du fichier personnel :</source>
        <translation>Name of personal file :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="12720"/>
        <source>Nombre de révolutions par jour :</source>
        <translation>Number of revolutions per day :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="12755"/>
        <source>18.000000000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="12790"/>
        <source>00.000000000</source>
        <translation>00.000000000</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="12952"/>
        <location filename="previsat.ui" line="14190"/>
        <source>Magnitude maximale :</source>
        <translation>Maximal magnitude :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="8102"/>
        <location filename="previsat.ui" line="12990"/>
        <source>Recherche</source>
        <translation>Search</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="13031"/>
        <location filename="previsat.cpp" line="11446"/>
        <source>Évènements orbitaux</source>
        <translation>Orbital events</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="13135"/>
        <source>Évènements</source>
        <translation>Events</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="13150"/>
        <source>Passages aux noeuds</source>
        <translation>Passes to nodes</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="13169"/>
        <source>Passages aux quadrangles</source>
        <translation>Passes to quadrangles</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="13188"/>
        <source>Passages apogée/périgée</source>
        <translation>Passes to apogee/perigee</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="13207"/>
        <source>Passages ombre/pénombre/lumière</source>
        <translation>Passes to shadow/penumbra/light</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="13226"/>
        <source>Transitions jour/nuit</source>
        <translation>Day/night transitions</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="13324"/>
        <location filename="previsat.cpp" line="11715"/>
        <source>Transits ISS</source>
        <translation>ISS transits</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="13470"/>
        <source>Age maximal du TLE :</source>
        <translation>TLE maximal age :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="13595"/>
        <source>Élongation maximale avec le corps :</source>
        <translation>Maximal elongation with the body :</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="13639"/>
        <source>Corps</source>
        <translation>Body</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="2432"/>
        <source>Aide</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="11631"/>
        <source>Mettre à jour maintenant</source>
        <translation>Update now</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14783"/>
        <source>Ajouter sélection dans</source>
        <translation>Add selection in</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14839"/>
        <location filename="previsat.cpp" line="7297"/>
        <location filename="previsat.cpp" line="7737"/>
        <location filename="previsat.cpp" line="10141"/>
        <location filename="previsat.cpp" line="10160"/>
        <location filename="previsat.cpp" line="10338"/>
        <location filename="previsat.cpp" line="10937"/>
        <location filename="previsat.cpp" line="11485"/>
        <location filename="previsat.cpp" line="11754"/>
        <source>Ouvrir fichier TLE</source>
        <translation>Open TLE file</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14859"/>
        <source>Fichier d&apos;aide</source>
        <translation>Help contents</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14867"/>
        <source>astropedia.free.fr</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14872"/>
        <source>Rapport de bug</source>
        <translation>Bug report</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14894"/>
        <source>www.celestrak.com</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14899"/>
        <source>www.space-track.org</source>
        <translation></translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14904"/>
        <source>À propos</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14909"/>
        <source>Définir par défaut</source>
        <translation>Define as default</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14914"/>
        <source>Nouveau fichier TLE</source>
        <translation>New TLE file</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14919"/>
        <source>Fichier TLE existant</source>
        <translation>Existing TLE file</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14929"/>
        <source>Aucun</source>
        <translation>None</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14934"/>
        <location filename="previsat.cpp" line="799"/>
        <source>Créer une catégorie</source>
        <translation>Create a category</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14939"/>
        <location filename="previsat.ui" line="14959"/>
        <location filename="previsat.ui" line="15018"/>
        <source>Supprimer</source>
        <translation>Remove</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14949"/>
        <location filename="previsat.cpp" line="802"/>
        <source>Créer un nouveau lieu</source>
        <translation>Create a new location</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14954"/>
        <source>Ajouter à Mes Préférés</source>
        <translation>Add in My Favorites</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14964"/>
        <source>Copier dans le presse-papier</source>
        <translation>Copy to the clipboard</translation>
    </message>
    <message>
        <location filename="previsat.ui" line="14847"/>
        <source>Enregistrer</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="435"/>
        <source>Erreur rencontrée lors de l&apos;initialisation
Le répertoire %1 n&apos;existe pas, veuillez réinstaller %2</source>
        <translation>Error occured while initialization
The directory %1 does not exist, please re-install %2</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="461"/>
        <location filename="previsat.cpp" line="471"/>
        <source>Le fichier %1 n&apos;existe pas, veuillez réinstaller %2</source>
        <translation>The file %1 does not exist, please re-install %2</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="1067"/>
        <source>Mise à jour automatique des TLE</source>
        <translation>Automatic update of TLE</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="1270"/>
        <location filename="previsat.cpp" line="1327"/>
        <source>Erreur rencontrée lors de l&apos;initialisation
Il n&apos;existe aucun fichier de lieux d&apos;observation</source>
        <translation>Error occured while initialization
There is no observation files</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="1302"/>
        <location filename="previsat.cpp" line="1303"/>
        <source>Mes Préférés</source>
        <translation>My Favorites</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="1332"/>
        <source>Erreur rencontrée lors de l&apos;initialisation
Le répertoire contenant les fichiers de lieux d&apos;observation n&apos;existe pas</source>
        <translation>Error occured while initialization
The directory containing the observation sites does not exist</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="1224"/>
        <location filename="previsat.cpp" line="1354"/>
        <source>* Défaut</source>
        <translation>* Default</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="2753"/>
        <location filename="previsat.cpp" line="8941"/>
        <source>UTC</source>
        <translation>UTC</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="2815"/>
        <location filename="previsat.cpp" line="3828"/>
        <source>%1 jours</source>
        <translation>%1 days</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="2871"/>
        <source>Ascendant</source>
        <translation>Ascending</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="2898"/>
        <location filename="previsat.cpp" line="2913"/>
        <location filename="previsat.cpp" line="2980"/>
        <source>Satellite non visible (Ombre)</source>
        <translation>Satellite not visible (Shadow)</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="2900"/>
        <source>Illumination : %1%</source>
        <translation>Illumination : %1%</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="2903"/>
        <location filename="previsat.cpp" line="2983"/>
        <source>Pénombre</source>
        <translation>Penumbra</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="2915"/>
        <source>Magnitude (Illumination) : %1%2</source>
        <translation>Magnitude (Illumination) : %1%2</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="2978"/>
        <source>Satellite non visible</source>
        <translation>Satellite not visible</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="2775"/>
        <location filename="previsat.cpp" line="5377"/>
        <source>Jour</source>
        <translation>Day</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="2777"/>
        <source>Crépuscule civil</source>
        <translation>Civil twilight</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="2779"/>
        <source>Crépuscule nautique</source>
        <translation>Nautical twilight</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="2783"/>
        <source>Nuit</source>
        <translation>Night</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="3039"/>
        <source>UA</source>
        <translation>AU</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="3188"/>
        <location filename="previsat.cpp" line="3587"/>
        <location filename="previsat.cpp" line="4442"/>
        <location filename="previsat.cpp" line="8468"/>
        <location filename="previsat.cpp" line="9502"/>
        <location filename="previsat.cpp" line="9606"/>
        <source>ft</source>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5488"/>
        <location filename="previsat.cpp" line="5503"/>
        <location filename="previsat.cpp" line="5556"/>
        <location filename="previsat.cpp" line="5653"/>
        <source>Erreur lors du téléchargement du fichier %1</source>
        <translation>Error while downloading file %1</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4930"/>
        <source>Longitude : %1	Hauteur    : %2	Ascension droite :  %3</source>
        <translation>Longitude : %1	Elevation   : %2	Right ascension  : %3</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="6065"/>
        <source>Fichiers PNG (*.png);;Fichiers JPEG (*.jpg);;Fichiers BMP (*.bmp);;Tous les fichiers (*)</source>
        <translation>PNG files (*.png);;JPEG files (*.jpg);;BMP files (*.bmp);;All files (*)</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="6733"/>
        <source>Réduire</source>
        <translation>Minimize</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="7299"/>
        <location filename="previsat.cpp" line="10162"/>
        <location filename="previsat.cpp" line="10340"/>
        <source>Fichiers texte (*.txt);;Fichiers TLE (*.tle);;Fichiers gz (*.gz);;Tous les fichiers (*)</source>
        <translation>Text files (*.txt);;TLE files (*.tle);;Gz files (*.gz);;All files (*)</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4625"/>
        <source>Ouverture du fichier TLE...</source>
        <translation>Opening TLE file...</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4631"/>
        <source>Fichier TLE OK : %1 satellites</source>
        <translation>TLE file OK : %1 satellites</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="7335"/>
        <location filename="previsat.cpp" line="10364"/>
        <source>Enregistrer sous...</source>
        <translation>Save as...</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="7336"/>
        <location filename="previsat.cpp" line="10365"/>
        <source>Fichiers texte (*.txt);;Tous les fichiers (*)</source>
        <translation>Text files (*.txt);;All files (*)</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="6784"/>
        <source>Carte du monde</source>
        <translation>World map</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="6363"/>
        <source>Longitude</source>
        <translation>Longitude</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="6364"/>
        <source>Latitude</source>
        <translation>Latitude</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="733"/>
        <location filename="previsat.cpp" line="6378"/>
        <location filename="previsat.cpp" line="6471"/>
        <location filename="previsat.cpp" line="6571"/>
        <location filename="previsat.cpp" line="7948"/>
        <location filename="previsat.cpp" line="10605"/>
        <location filename="previsat.cpp" line="11291"/>
        <source>%1 (numéro NORAD : %2)</source>
        <translation>%1 (NORAD number : %2)</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="6455"/>
        <source>Azimut</source>
        <translation>Azimuth</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="6456"/>
        <source>Hauteur</source>
        <translation>Elevation</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="7702"/>
        <location filename="previsat.cpp" line="7738"/>
        <location filename="previsat.cpp" line="10143"/>
        <location filename="previsat.cpp" line="10939"/>
        <location filename="previsat.cpp" line="11487"/>
        <location filename="previsat.cpp" line="11756"/>
        <source>Fichiers texte (*.txt);;Fichiers TLE (*.tle);;Tous les fichiers (*)</source>
        <translation>Text files (*.txt);;TLE files (*.tle);;All files (*)</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="7722"/>
        <source>Fichier %1 créé</source>
        <translation>File %1 created</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="7802"/>
        <source>Fichier %1 augmenté de nouveaux satellites</source>
        <translation>New satellites added in file %1</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4185"/>
        <location filename="previsat.cpp" line="4253"/>
        <location filename="previsat.cpp" line="9232"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5547"/>
        <source>Ajout du fichier %1</source>
        <translation>Addition of file %1</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4251"/>
        <source>Les éléments orbitaux sont plus vieux que %1 jour(s). Souhaitez-vous les mettre à jour?</source>
        <translation>The orbital elements are older than %1 day(s). Do you want to update them?</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="573"/>
        <source>Accepter ajout/suppression de TLE</source>
        <translation>Allow TLE add/remove</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="574"/>
        <source>Refuser ajout/suppression de TLE</source>
        <translation>Disallow TLE add/remove</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4777"/>
        <location filename="previsat.cpp" line="4928"/>
        <location filename="previsat.cpp" line="5014"/>
        <source>Nom du satellite :</source>
        <translation>Name of satellite :</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4934"/>
        <source>Latitude  :  %1	Azimut (N) : %2	Déclinaison      : %3</source>
        <translation>Latitude  :  %1	Azimuth (N) : %2	Declination      : %3</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4938"/>
        <source>Altitude  :  %1%2	Distance   : %3%4	Constellation    : %5</source>
        <translation>Altitude  :  %1%2	Range       : %3%4	Constellation    : %5</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4943"/>
        <source>Direction          : %1  	%2</source>
        <translation>Direction        : %1  	%2</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4957"/>
        <location filename="previsat.cpp" line="4968"/>
        <source>Hauteur    : %1	Ascension droite :  %2</source>
        <translation>Elevation   : %1	Right ascension :  %2</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4960"/>
        <location filename="previsat.cpp" line="4971"/>
        <source>Azimut (N) : %1	Déclinaison      : %2</source>
        <translation>Azimuth (N) : %1	Declination     : %2</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4963"/>
        <source>Distance   : %1   	Constellation    : %2</source>
        <translation>Range       : %1   	Constellation   : %2</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4974"/>
        <source>Distance   : %1  	Constellation    : %2</source>
        <translation>Range       : %1  	Constellation   : %2</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4976"/>
        <source>Phase        :</source>
        <translation>Phase        :</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5029"/>
        <source>Coeff pseudo-balistique : %1 (1/Re)	Nb orbites à l&apos;époque : %2			 Site de lancement  : %3</source>
        <translation>Pseudo-ballistic coeff : %1 (1/Re)	Orbit # at epoch : %2			Launch site          : %3</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5032"/>
        <source>Inclinaison             : %1%2		Anomalie moyenne      : %3%4</source>
        <translation>Inclination         : %1%2 		Mean anomaly       : %3%4</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5037"/>
        <source>AD noeud ascendant      : %1%2		Magnitude std/max     : %3</source>
        <translation>RA ascending node   : %1%2 		Std/Max magnitude  : %3</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5041"/>
        <source>Excentricité            : %1		Modèle orbital        : %2</source>
        <translation>Eccentricity        : %1 		Propagation model  : %2</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5044"/>
        <source>Argument du périgée     : %1%2		Dimensions/Section    : %3%4</source>
        <translation>Argument of perigee : %1%2 		Dimensions/Section : %3%4</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5382"/>
        <source>dd/MM/yyyy</source>
        <translation>MM/dd/yyyy</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="7098"/>
        <source>Impossible de lancer le flux vidéo : essayez de nouveau et/ou vérifiez votre connexion Internet</source>
        <translation>Impossible to launch the video stream : try again and/or check your Internet connexion</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="7277"/>
        <source>Impossible d&apos;ouvrir le fichier d&apos;aide %1</source>
        <translation>Impossible to open the help file %1</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="7325"/>
        <source>onglet_elements</source>
        <translation>elements_tab</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="7326"/>
        <source>onglet_informations</source>
        <translation>informations_tab</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="9283"/>
        <source>Le nom de la catégorie n&apos;est pas spécifié</source>
        <translation>The name of the category is not given</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="9303"/>
        <source>La nouvelle catégorie de lieux d&apos;observation a été créée</source>
        <translation>The new category of locations has been created</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="9307"/>
        <source>La catégorie spécifiée existe déjà</source>
        <translation>The category already exists</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="9323"/>
        <source>Catégorie</source>
        <translation>Category</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="9323"/>
        <source>Nouveau nom de la catégorie :</source>
        <translation>New name of the category :</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="9658"/>
        <source>Le nom du lieu d&apos;observation n&apos;est pas spécifié</source>
        <translation>The name of location is not given</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="9670"/>
        <source>Erreur dans la saisie de la longitude</source>
        <translation>Error in longitude input</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="9678"/>
        <source>Erreur dans la saisie de la latitude</source>
        <translation>Error in latitude input</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="9685"/>
        <source>Erreur dans la saisie de l&apos;altitude</source>
        <translation>Error in altitude input</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="9230"/>
        <source>Voulez-vous vraiment supprimer la catégorie &quot;%1&quot;?</source>
        <translation>Do you really want  to remove the &quot;%1&quot; category?</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="868"/>
        <source>Messages</source>
        <translation>Messages</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="3010"/>
        <source>%1 (dans %2). Azimut : %3</source>
        <translation>%1 (in %2). Azimuth : %3</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="3014"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="3015"/>
        <location filename="previsat.cpp" line="3016"/>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="3017"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4950"/>
        <source>Variation distance : %1  	%2 %3</source>
        <translation>Range rate       : %1 	%2 %3</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="9801"/>
        <source>Avertissement</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="9247"/>
        <source>La catégorie &quot;%1&quot; a été supprimée</source>
        <translation>The category &quot;%1&quot; has been removed</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="9551"/>
        <source>Le lieu d&apos;observation &quot;%1&quot; fait déjà partie de &quot;Mes Préférés&quot;</source>
        <translation>The location &quot;%1&quot; is already in &quot;My Favorites&quot;</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="9696"/>
        <source>Le lieu existe déjà dans la catégorie &quot;%1&quot;</source>
        <translation>The location already exists in the category &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="9799"/>
        <source>Voulez-vous vraiment supprimer &quot;%1&quot; de la catégorie &quot;%2&quot;?</source>
        <translation>Do you really want to remove &quot;%1&quot; from the category &quot;%2&quot;?</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="9840"/>
        <source>Le lieu d&apos;observation &quot;%1&quot; a été supprimé de la catégorie &quot;%2&quot;</source>
        <translation>The location %1 has been removed from the category %2</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="9866"/>
        <source>Lieu d&apos;observation déjà sélectionné</source>
        <translation>Location already selected</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="10187"/>
        <source>Le nom du fichier à mettre à jour n&apos;est pas spécifié</source>
        <translation>The name of file to update is not given</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="10190"/>
        <location filename="previsat.cpp" line="10389"/>
        <source>Le nom du fichier à lire n&apos;est pas spécifié</source>
        <translation>The name of file to read is not given</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4467"/>
        <source>TLE du satellite %1 (%2) non réactualisé</source>
        <translation>TLE of satellite %1 (%2) not updated</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4463"/>
        <source>%1 TLE(s) sur %2 mis à jour</source>
        <translation>%1 TLE(s) on %2 updated</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4477"/>
        <source>Mise à jour de tous les TLE effectuée (fichier de %1 satellite(s))</source>
        <translation>All TLE(s) updated (file of %1 satellite(s))</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4482"/>
        <source>Aucun TLE mis à jour</source>
        <translation>No TLE updated</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4485"/>
        <source>Nombre de TLE(s) supprimés : %1</source>
        <translation>Number of TLE(s) removed : %1</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4490"/>
        <source>Nombre de TLE(s) ajoutés : %1</source>
        <translation>Number of TLE(s) added : %1</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5299"/>
        <location filename="previsat.cpp" line="5623"/>
        <location filename="previsat.cpp" line="10237"/>
        <source>Terminé !</source>
        <translation>Done !</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="9753"/>
        <source>Nouveau nom du lieu d&apos;observation :</source>
        <translation>New name of the location :</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4462"/>
        <source>Fichier</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="742"/>
        <source>Satellite non opérationnel</source>
        <translation>Non-operational satellite</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="742"/>
        <source>Satellite de réserve</source>
        <translation>Spare satellite</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="743"/>
        <source>Satellite opérationnel</source>
        <translation>Operational satellite</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="965"/>
        <location filename="previsat.cpp" line="4613"/>
        <location filename="previsat.cpp" line="4620"/>
        <location filename="previsat.cpp" line="10204"/>
        <location filename="previsat.cpp" line="10209"/>
        <location filename="previsat.cpp" line="10409"/>
        <location filename="previsat.cpp" line="10414"/>
        <source>Erreur rencontrée lors de la décompression du fichier %1</source>
        <translation>Error occured while decompressing file %1</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="2718"/>
        <location filename="previsat.cpp" line="3323"/>
        <location filename="previsat.cpp" line="8501"/>
        <location filename="previsat.cpp" line="10866"/>
        <location filename="previsat.cpp" line="11150"/>
        <location filename="previsat.cpp" line="11415"/>
        <location filename="previsat.cpp" line="11658"/>
        <location filename="previsat.cpp" line="11953"/>
        <source>nmi</source>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="2719"/>
        <location filename="previsat.cpp" line="3324"/>
        <source>km/s</source>
        <translation>km/s</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="2719"/>
        <location filename="previsat.cpp" line="3324"/>
        <source>nmi/s</source>
        <translation>nmi/s</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="3006"/>
        <source>Prochain %1 :</source>
        <translation>Next %1 :</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="3174"/>
        <location filename="previsat.cpp" line="8453"/>
        <source>SGP4 (DS)</source>
        <translation>SGP4 (DS)</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="3193"/>
        <location filename="previsat.cpp" line="8473"/>
        <source>Sphérique. R=</source>
        <translation>Spherical. R=</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="3195"/>
        <location filename="previsat.cpp" line="8476"/>
        <source>Cylindrique. L=</source>
        <translation>Cylindrical. L=</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="3196"/>
        <location filename="previsat.cpp" line="8477"/>
        <source>, R=</source>
        <translation>, R=</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="3198"/>
        <location filename="previsat.cpp" line="8480"/>
        <source>Boîte.</source>
        <translation>Box.</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="3202"/>
        <location filename="previsat.cpp" line="5047"/>
        <location filename="previsat.cpp" line="5062"/>
        <location filename="previsat.cpp" line="8485"/>
        <source>Inconnues</source>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="3222"/>
        <source>Non connue</source>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="3227"/>
        <source>Indéterminée</source>
        <translation>Undetermined</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="3230"/>
        <source>Indéterminé</source>
        <translation>Undetermined</translation>
    </message>
    <message>
        <source>AOS</source>
        <translation type="vanished">AOS</translation>
    </message>
    <message>
        <source>LOS</source>
        <translation type="vanished">LOS</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4319"/>
        <source>de </source>
        <translation>of </translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4351"/>
        <source>des fichiers internes</source>
        <translation>of internal files</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4183"/>
        <source>Une mise à jour %1 est disponible. Souhaitez-vous la télécharger?</source>
        <translation>An update %1 is available. Do you want to download it?</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4766"/>
        <location filename="previsat.cpp" line="4909"/>
        <location filename="previsat.cpp" line="5002"/>
        <location filename="previsat.cpp" line="10502"/>
        <source>Problème de droits d&apos;écriture du fichier %1</source>
        <translation>Problem with writing rights with file %1</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4920"/>
        <source>Longitude  : %1	Latitude : %2	Altitude : %3</source>
        <translation>Longitude  : %1	Latitude : %2	Altitude : %3</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4922"/>
        <source>Conditions : %1</source>
        <translation>Conditions : %1</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4946"/>
        <source>Vitesse orbitale   : %1%2  	Orbite n°%3</source>
        <translation>Orbital velocity : %1%2	Orbit #%3</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4978"/>
        <source>Magnitude    :</source>
        <translation>Magnitude    :</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4782"/>
        <source>x : %1%2	vx : %3</source>
        <translation>x : %1%2	vx : %3</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="741"/>
        <source>Non op.</source>
        <translation>Non Op.</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="741"/>
        <source>Op.</source>
        <translation>Op.</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="741"/>
        <source>Sp.</source>
        <translation>Sp.</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4113"/>
        <source>Mise à jour du fichier TLE %1 en cours</source>
        <translation>Updating TLE file %1</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4187"/>
        <location filename="previsat.cpp" line="4255"/>
        <location filename="previsat.cpp" line="9234"/>
        <location filename="previsat.cpp" line="9803"/>
        <source>Oui</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4188"/>
        <location filename="previsat.cpp" line="4256"/>
        <location filename="previsat.cpp" line="9235"/>
        <location filename="previsat.cpp" line="9804"/>
        <source>Non</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4786"/>
        <source>y : %1%2	vy : %3</source>
        <translation>y : %1%2	vy : %3</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4790"/>
        <source>z : %1%2	vz : %3</source>
        <translation>z : %1%2	vz : %3</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4800"/>
        <location filename="previsat.cpp" line="4818"/>
        <source>Demi-grand axe : %1	Ascension droite du noeud ascendant : %2%3</source>
        <translation>Semi-major axis : %1	Right ascension of the ascending node : %2%3</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4805"/>
        <source>Excentricité   : %1	Argument du périgée                 : %2%3</source>
        <translation>Eccentricity    : %1	Argument of perigee                   : %2%3</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4810"/>
        <source>Inclinaison    : %1%2	Anomalie moyenne                    : %3%4</source>
        <translation>Inclination     : %1%2 	Mean anomaly                          : %3%4</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4823"/>
        <source>Ex             : %1	Inclinaison                         : %2%3</source>
        <translation>Ex              : %1	Inclination                           : %2%3</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4827"/>
        <source>Ey             : %1	Position sur orbite                 : %2%3</source>
        <translation>Ey              : %1	Position on orbit                     : %2%3</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4835"/>
        <source>Demi-grand axe       : %1	Ix               : %2</source>
        <translation>Semi-major axis      : %1 	Ix           : %2</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4838"/>
        <source>Excentricité         : %1	Iy               : %2</source>
        <translation>Eccentricity         : %1 	Iy           : %2</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4841"/>
        <source>Longitude du périgée : %1%2	Anomalie moyenne : %3%4</source>
        <translation>Longitude of perigee : %1%2 	Mean anomaly : %3%4</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4850"/>
        <source>Demi-grand axe : %1	Ix                         : %2</source>
        <translation>Semi-major axis : %1	Ix                       : %2</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4853"/>
        <source>Ex             : %1	Iy                         : %2</source>
        <translation>Ex              : %1	Iy                       : %2</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4856"/>
        <source>Ey             : %1	Argument de longitude vrai : %2%3</source>
        <translation>Ey              : %1	True longtitude argument : %2%3</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4865"/>
        <source>Anomalie vraie       : %1%2	Apogée  (Altitude) : %3</source>
        <translation>True anomaly      : %1%2	Apogee  (Altitude) : %3</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4869"/>
        <source>Anomalie excentrique : %1%2	Périgée (Altitude) : %3</source>
        <translation>Eccentric anomaly : %1%2	Perigee (Altitude) : %3</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4873"/>
        <source>Champ de vue         : %1  	Période orbitale   : %2</source>
        <translation>Field of view     : %1  	Orbital period     : %2</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4877"/>
        <source>Signal :</source>
        <translation>Signal :</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4878"/>
        <source>Doppler @ 100 MHz    : %1</source>
        <translation>Doppler @ 100 MHz    : %1</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4881"/>
        <source>Atténuation          : %1</source>
        <translation>Fre-space loss       : %1</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4884"/>
        <source>Délai                : %1</source>
        <translation>Delay                : %1</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5018"/>
        <source>Numéro NORAD            : %1 		Moyen mouvement       : %2 rev/jour	 Date de lancement  : %3</source>
        <translation>NORAD number           : %1  		Mean motion      : %2 rev/day		Launch date          : %3</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5021"/>
        <source>Désignation COSPAR      : %1		n&apos;/2                  : %2%3 rev/jour^2	 Catégorie d&apos;orbite : %4</source>
        <translation>COSPAR Designation     : %1 		n&apos;/2             : %2%3 rev/day^2	Orbital category     : %4</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5025"/>
        <source>Époque (UTC)            : %1	n&quot;/6                  : %2%3 rev/jour^3	 Pays/Organisation  : %4</source>
        <translation>Epoch (UTC)            : %1	n&quot;/6             : %2%3 rev/day^3	Country/Organization : %4</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5052"/>
        <source>Nom                :</source>
        <translation>Name                 :</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5054"/>
        <source>Numéro NORAD       : %1		Magnitude std/max  : %2</source>
        <translation>NORAD Number         : %1		Std/Max magnitude  : %2</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5057"/>
        <source>Désignation COSPAR : %1		Modèle orbital     : %2</source>
        <translation>COSPAR designation   : %1	Propagation model  : %2</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5060"/>
        <source>Dimensions/Section : %1%2</source>
        <translation>Dimensions/Section   : %1%2</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5064"/>
        <source>Date de lancement  : %1		Apogée  (Altitude) : %2</source>
        <translation>Launch date          : %1	Apogee  (Altitude) : %2</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5067"/>
        <source>Date de rentrée    : %1		</source>
        <translation>Decay date           : %1	</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5068"/>
        <location filename="previsat.cpp" line="5071"/>
        <source>Catégorie d&apos;orbite : %1		</source>
        <translation>Orbital category     : %1		</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5069"/>
        <source>Périgée (Altitude) : %1</source>
        <translation>Perigee (Altitude) : %1</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5072"/>
        <location filename="previsat.cpp" line="5075"/>
        <source>Pays/Organisation  : %1		</source>
        <translation>Country/Organization : %1		</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5073"/>
        <source>Période orbitale   : %1</source>
        <translation>Orbital period     : %1</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5076"/>
        <source>Site de lancement  : %1		</source>
        <translation>Launch site          : %1		</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5077"/>
        <source>Inclinaison        : %1</source>
        <translation>Inclination        : %1</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5080"/>
        <source>Site de lancement  : %1</source>
        <translation>Launch site          : %1</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="6798"/>
        <location filename="previsat.cpp" line="6804"/>
        <source>seconde</source>
        <translation>second</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="6805"/>
        <source>minute</source>
        <translation>minute</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="6806"/>
        <source>heure</source>
        <translation>hour</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="6807"/>
        <source>jour</source>
        <translation>day</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="7030"/>
        <location filename="previsat.cpp" line="7200"/>
        <source>Cliquez ici pour activer
le flux vidéo</source>
        <translation>Click here to activate
the video stream</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="7109"/>
        <source>Veuillez patienter...</source>
        <translation>Please wait...</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="7325"/>
        <source>onglet_general</source>
        <translation>main_tab</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="7820"/>
        <location filename="previsat.cpp" line="10220"/>
        <location filename="previsat.cpp" line="10419"/>
        <source>Le fichier %1 n&apos;existe pas</source>
        <translation>The file %1 does not exist</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="3233"/>
        <location filename="previsat.cpp" line="8426"/>
        <location filename="previsat.cpp" line="8503"/>
        <location filename="previsat.cpp" line="8515"/>
        <location filename="previsat.cpp" line="8581"/>
        <location filename="previsat.cpp" line="8585"/>
        <source>Inconnu</source>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="8433"/>
        <location filename="previsat.cpp" line="8524"/>
        <location filename="previsat.cpp" line="8529"/>
        <location filename="previsat.cpp" line="8539"/>
        <location filename="previsat.cpp" line="8577"/>
        <source>Inconnue</source>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="8454"/>
        <source>Non applicable</source>
        <translation>Not applicable</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="8605"/>
        <source>Objets trouvés (%1) :</source>
        <translation>Found objects (%1) :</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="9568"/>
        <source>Le lieu d&apos;observation &quot;%1&quot; a été ajouté dans la catégorie &quot;Mes Préférés&quot;</source>
        <translation>The location &quot;%1&quot; has been added in &quot;My Favorites&quot; category</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="10479"/>
        <source>Erreur rencontrée lors du chargement du fichier %1</source>
        <translation>Error while downloading file %1</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="10755"/>
        <location filename="previsat.cpp" line="11340"/>
        <source>Aucun satellite n&apos;est sélectionné dans la liste</source>
        <translation>None satellite selected in the list</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="11057"/>
        <location filename="previsat.cpp" line="11587"/>
        <location filename="previsat.cpp" line="11870"/>
        <source>Le nom du fichier TLE n&apos;est pas spécifié</source>
        <translation>The name of TLE file is not given</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="11062"/>
        <location filename="previsat.cpp" line="11592"/>
        <location filename="previsat.cpp" line="11875"/>
        <source>Le nom du fichier TLE est incorrect</source>
        <translation>The name of TLE file is incorrect</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="11157"/>
        <source>Erreur rencontrée lors de l&apos;exécution
Aucun satellite Iridium susceptible de produire des flashs dans le fichier de statut</source>
        <translation>Error occured while execution
There is no Iridium satellites which can make flares in the Iridium status file</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="11177"/>
        <location filename="previsat.cpp" line="11665"/>
        <location filename="previsat.cpp" line="11974"/>
        <source>Erreur rencontrée lors du chargement du fichier
Le fichier %1 n&apos;est pas un TLE</source>
        <translation>Error occured while loading file
The file %1 is not a TLE</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="11204"/>
        <source>Erreur rencontrée lors de l&apos;exécution
Aucun satellite Iridium n&apos;a été trouvé dans le fichier TLE</source>
        <translation>Error occured while execution
There is no Iridium satellites in the TLE file</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="11387"/>
        <source>Aucun évènement sélectionné</source>
        <translation>No event selected</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="11673"/>
        <source>Erreur rencontrée lors du chargement du fichier
Le fichier %1 ne contient pas le TLE de l&apos;ISS</source>
        <translation>Error occured while loading file
The file %1 does not contain the TLE of ISS</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="11681"/>
        <source>L&apos;âge du TLE de l&apos;ISS (%1 jours) est supérieur à %2 jours</source>
        <translation>The TLE age of ISS (%1 days) is over to %2 days</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="11941"/>
        <source>metop_skymed</source>
        <translation>metop_skymed</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="11960"/>
        <source>Erreur rencontrée lors de l&apos;exécution
Aucun satellite MetOp ou SkyMed susceptible de produire des flashs dans le fichier de statut</source>
        <translation>Error occured while execution
There is no MetOp or SkyMed satellites which can make flares in the status file</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="12001"/>
        <source>Erreur rencontrée lors de l&apos;exécution
Aucun satellite MetOp ou SkyMed n&apos;a été trouvé dans le fichier TLE</source>
        <translation>Error occured while execution
There is no MetOp or SkyMed satellites in the TLE file</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="12034"/>
        <source>Prévisions des flashs MetOp et SkyMed</source>
        <translation>Predictions of MetOp and SkyMed flares</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="10392"/>
        <source>Le nom du fichier personnel n&apos;est pas spécifié</source>
        <translation>The name of personal file is not given</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="10552"/>
        <source>Fichier %1 écrit</source>
        <translation>File %1 created</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="10854"/>
        <source>previsions</source>
        <translation>predictions</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="2718"/>
        <location filename="previsat.cpp" line="3323"/>
        <location filename="previsat.cpp" line="8501"/>
        <location filename="previsat.cpp" line="10866"/>
        <location filename="previsat.cpp" line="11150"/>
        <location filename="previsat.cpp" line="11415"/>
        <location filename="previsat.cpp" line="11658"/>
        <location filename="previsat.cpp" line="11953"/>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="10868"/>
        <location filename="previsat.cpp" line="11207"/>
        <location filename="previsat.cpp" line="11417"/>
        <location filename="previsat.cpp" line="11685"/>
        <location filename="previsat.cpp" line="12004"/>
        <source>Calculs en cours. Veuillez patienter...</source>
        <translation>Calculating. Be patient...</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="10898"/>
        <source>Prévisions de passage des satellites</source>
        <translation>Predictions of satellite passes</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="11138"/>
        <source>iridiums</source>
        <translation>iridiums</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="11237"/>
        <source>Prévisions des flashs Iridium</source>
        <translation>Predictions of Iridium flares</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="11403"/>
        <source>evenements</source>
        <translation>events</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="11646"/>
        <source>transits</source>
        <translation>transits</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="4053"/>
        <location filename="previsat.cpp" line="7892"/>
        <source>Erreur rencontrée lors de l&apos;exécution
La position du satellite %1 (numéro NORAD : %2) ne peut pas être calculée (altitude négative)</source>
        <translation>Error occured while execution
The position of the satellite %1 (NORAD number : %2) can not be calculated (negative altitude)</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="5376"/>
        <source>Jour julien</source>
        <translation>Julian day</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="896"/>
        <location filename="previsat.cpp" line="5384"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="903"/>
        <location filename="previsat.cpp" line="5385"/>
        <source>Heure</source>
        <translation>Hour</translation>
    </message>
    <message>
        <location filename="previsat.cpp" line="6064"/>
        <location filename="previsat.cpp" line="7701"/>
        <source>Enregistrer sous</source>
        <translation>Save as</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="librairies/corps/satellite/tle.cpp" line="197"/>
        <source>La longueur des lignes du TLE du satellite %1 (numéro NORAD : %2) est incorrecte</source>
        <translation>The length of lines of TLE of the satellite %1 (NORAD number %2) is incorrect</translation>
    </message>
    <message>
        <location filename="librairies/corps/satellite/tle.cpp" line="202"/>
        <source>Les numéros de ligne du TLE du satellite %1 (numéro NORAD : %2 ) sont incorrects</source>
        <translation>The numbers of the lines of TLE of satellite %1 (NORAD number : %2) are incorrect</translation>
    </message>
    <message>
        <location filename="librairies/corps/satellite/tle.cpp" line="207"/>
        <source>Erreur position des espaces du TLE :
Satellite %1 - numéro NORAD : %2</source>
        <translation>Error of position of spaces in TLE :
Satellite %1 - NORAD number : %2</translation>
    </message>
    <message>
        <location filename="librairies/corps/satellite/tle.cpp" line="212"/>
        <source>Erreur Ponctuation du TLE :
Satellite %1 - numéro NORAD : %2</source>
        <translation>Punctuation Error of TLE :
Satellite %1 - NORAD number : %2</translation>
    </message>
    <message>
        <location filename="librairies/corps/satellite/tle.cpp" line="217"/>
        <source>Les deux lignes du TLE du satellite %1 ont des numéros NORAD différents (%2 et %3)</source>
        <translation>The two lines of TLE of satellite %1 have different NORAD numbers (%2 and %3)</translation>
    </message>
    <message>
        <location filename="librairies/corps/satellite/tle.cpp" line="223"/>
        <source>Erreur CheckSum ligne %1 :
Satellite %2 - numéro NORAD : %3</source>
        <translation>CheckSum Error line %1 :
Satellite %2 - NORAD number : %3</translation>
    </message>
    <message>
        <location filename="librairies/corps/satellite/tle.cpp" line="228"/>
        <source>Le fichier %1 ne contient aucun satellite ou n&apos;est pas valide</source>
        <translation>The file %1 does not contain satellites or is not valid</translation>
    </message>
    <message>
        <location filename="librairies/corps/satellite/tle.cpp" line="368"/>
        <location filename="librairies/corps/satellite/tle.cpp" line="377"/>
        <source>Erreur rencontrée lors du chargement du fichier
Le fichier %1 n&apos;est pas un TLE</source>
        <translation>Error occured while loading file
The file %1 is not a TLE</translation>
    </message>
    <message>
        <location filename="librairies/corps/satellite/tle.cpp" line="428"/>
        <source>Le satellite %1 (numéro NORAD : %2) n&apos;existe pas dans le fichier à mettre à jour.
Voulez-vous ajouter ce TLE dans le fichier à mettre à jour ?</source>
        <translation>The satellite %1 (NORAD number : %2) does not exist in the file to update.
Do you want to add this TLE in the file to update?</translation>
    </message>
    <message>
        <location filename="librairies/corps/satellite/tle.cpp" line="432"/>
        <source>Ajout du nouveau TLE</source>
        <translation>Addition of the new TLE</translation>
    </message>
    <message>
        <location filename="librairies/corps/satellite/tle.cpp" line="437"/>
        <location filename="librairies/corps/satellite/tle.cpp" line="463"/>
        <source>Oui</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="librairies/corps/satellite/tle.cpp" line="438"/>
        <location filename="librairies/corps/satellite/tle.cpp" line="464"/>
        <source>Oui à tout</source>
        <translation>Yes to all</translation>
    </message>
    <message>
        <location filename="librairies/corps/satellite/tle.cpp" line="439"/>
        <location filename="librairies/corps/satellite/tle.cpp" line="465"/>
        <source>Non</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="librairies/corps/satellite/tle.cpp" line="440"/>
        <location filename="librairies/corps/satellite/tle.cpp" line="466"/>
        <source>Non à tout</source>
        <translation>No to all</translation>
    </message>
    <message>
        <location filename="librairies/corps/satellite/tle.cpp" line="455"/>
        <source>Le satellite %1 (numéro NORAD : %2) n&apos;existe pas dans le fichier de TLE récents.
Voulez-vous supprimer ce TLE du fichier à mettre à jour ?</source>
        <translation>The satellite %1 (NORAD number : %2) does not exist in the TLE files to read.
Do you want to remove this TLE from the file to update?</translation>
    </message>
    <message>
        <location filename="librairies/corps/satellite/tle.cpp" line="458"/>
        <source>Suppression du TLE</source>
        <translation>Deletion of the TLE</translation>
    </message>
    <message>
        <location filename="librairies/corps/systemesolaire/lune.cpp" line="209"/>
        <source>Nouvelle Lune</source>
        <translation>New Moon</translation>
    </message>
    <message>
        <location filename="librairies/corps/systemesolaire/lune.cpp" line="212"/>
        <source>Premier croissant</source>
        <translation>Waxing crescent</translation>
    </message>
    <message>
        <location filename="librairies/corps/systemesolaire/lune.cpp" line="212"/>
        <source>Dernier croissant</source>
        <translation>Waning crescent</translation>
    </message>
    <message>
        <location filename="librairies/corps/systemesolaire/lune.cpp" line="215"/>
        <source>Premier quartier</source>
        <translation>First quarter</translation>
    </message>
    <message>
        <location filename="librairies/corps/systemesolaire/lune.cpp" line="215"/>
        <source>Dernier quartier</source>
        <translation>Last quarter</translation>
    </message>
    <message>
        <location filename="librairies/corps/systemesolaire/lune.cpp" line="218"/>
        <source>Gibbeuse croissante</source>
        <translation>Waxing crescent</translation>
    </message>
    <message>
        <location filename="librairies/corps/systemesolaire/lune.cpp" line="218"/>
        <source>Gibbeuse décroissante</source>
        <translation>Waning decrescent</translation>
    </message>
    <message>
        <location filename="librairies/corps/systemesolaire/lune.cpp" line="221"/>
        <source>Pleine Lune</source>
        <translation>Full Moon</translation>
    </message>
    <message>
        <location filename="librairies/exceptions/message.cpp" line="60"/>
        <location filename="main.cpp" line="91"/>
        <location filename="main.cpp" line="95"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="librairies/exceptions/message.cpp" line="64"/>
        <source>Avertissement</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="librairies/exceptions/message.cpp" line="68"/>
        <source>Erreur</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="librairies/maths/maths.cpp" line="137"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="librairies/maths/maths.cpp" line="138"/>
        <location filename="previsions/conditions.cpp" line="380"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="librairies/maths/maths.cpp" line="139"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="previsions/conditions.cpp" line="377"/>
        <location filename="previsions/conditions.cpp" line="380"/>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <location filename="previsions/evenements.cpp" line="206"/>
        <location filename="previsions/flashs.cpp" line="677"/>
        <location filename="previsions/prevision.cpp" line="219"/>
        <location filename="previsions/transitiss.cpp" line="272"/>
        <source>nmi</source>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="previsions/conditions.cpp" line="380"/>
        <source>ft</source>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="previsions/conditions.cpp" line="372"/>
        <source>Ouest</source>
        <translation>West</translation>
    </message>
    <message>
        <location filename="previsions/conditions.cpp" line="372"/>
        <source>Est</source>
        <translation>East</translation>
    </message>
    <message>
        <location filename="previsions/conditions.cpp" line="375"/>
        <source>Nord</source>
        <translation>North</translation>
    </message>
    <message>
        <location filename="previsions/conditions.cpp" line="375"/>
        <source>Sud</source>
        <translation>South</translation>
    </message>
    <message>
        <location filename="previsions/conditions.cpp" line="381"/>
        <source>Conditions d&apos;observations :</source>
        <translation>Conditions of observations :</translation>
    </message>
    <message>
        <location filename="previsions/conditions.cpp" line="382"/>
        <source>Hauteur minimale du satellite = %1°</source>
        <translation>Minimal elevation of the satellite = %1°</translation>
    </message>
    <message>
        <location filename="previsions/conditions.cpp" line="391"/>
        <source>Age du TLE                : %1 jours (au %2)</source>
        <translation>Age of the TLE             : %1 days (at %2)</translation>
    </message>
    <message>
        <location filename="previsions/conditions.cpp" line="417"/>
        <source>Age du TLE le plus récent : %1 jours (au %2)
Age du TLE le plus ancien : %3 jours</source>
        <translation>Age of the most recent TLE : %1 days (at %2)
Age of the oldest TLE      : %3 days</translation>
    </message>
    <message>
        <location filename="previsions/conditions.cpp" line="431"/>
        <source>Lieu d&apos;observation        : %1     %2 %3   %4 %5   %6 %7</source>
        <translation>Location                   : %1     %2 %3   %4 %5   %6 %7</translation>
    </message>
    <message>
        <location filename="previsions/conditions.cpp" line="436"/>
        <source>Fuseau horaire            : %1</source>
        <translation>Timezone                   : %1</translation>
    </message>
    <message>
        <location filename="previsions/conditions.cpp" line="437"/>
        <location filename="previsions/evenements.cpp" line="91"/>
        <source>UTC</source>
        <translation>UTC</translation>
    </message>
    <message>
        <location filename="previsions/conditions.cpp" line="445"/>
        <location filename="previsions/evenements.cpp" line="99"/>
        <source>Heure légale</source>
        <translation>Local time</translation>
    </message>
    <message>
        <location filename="previsions/conditions.cpp" line="451"/>
        <source>Hauteur maximale du Soleil = %1°</source>
        <translation>Maximal elevation of the Sun = %1°</translation>
    </message>
    <message>
        <location filename="previsions/conditions.cpp" line="455"/>
        <source>Unité de distance         : %1</source>
        <translation>Range unit                 : %1</translation>
    </message>
    <message>
        <location filename="previsions/evenements.cpp" line="141"/>
        <source>Noeud Ascendant - PSO = 0°</source>
        <translation>Ascending node - Position = 0°</translation>
    </message>
    <message>
        <location filename="previsions/evenements.cpp" line="142"/>
        <source>Noeud Descendant - PSO = 180°</source>
        <translation>Descending node - Position = 180°</translation>
    </message>
    <message>
        <location filename="previsions/evenements.cpp" line="212"/>
        <location filename="previsions/evenements.cpp" line="420"/>
        <location filename="previsions/flashs.cpp" line="716"/>
        <location filename="previsions/transitiss.cpp" line="325"/>
        <source>W</source>
        <translation>W</translation>
    </message>
    <message>
        <location filename="previsions/evenements.cpp" line="212"/>
        <location filename="previsions/evenements.cpp" line="420"/>
        <location filename="previsions/flashs.cpp" line="716"/>
        <location filename="previsions/transitiss.cpp" line="325"/>
        <source>E</source>
        <translation>E</translation>
    </message>
    <message>
        <location filename="previsions/evenements.cpp" line="214"/>
        <location filename="previsions/evenements.cpp" line="422"/>
        <location filename="previsions/flashs.cpp" line="717"/>
        <location filename="previsions/transitiss.cpp" line="326"/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="previsions/evenements.cpp" line="214"/>
        <location filename="previsions/evenements.cpp" line="422"/>
        <location filename="previsions/flashs.cpp" line="717"/>
        <location filename="previsions/transitiss.cpp" line="285"/>
        <location filename="previsions/transitiss.cpp" line="326"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="previsions/evenements.cpp" line="162"/>
        <source>Pénombre -&gt; Ombre</source>
        <translation>Penumbra -&gt; Shadow</translation>
    </message>
    <message>
        <location filename="previsions/evenements.cpp" line="90"/>
        <source>Fuseau horaire : %1</source>
        <translation>Timezone : %1</translation>
    </message>
    <message>
        <location filename="previsions/evenements.cpp" line="162"/>
        <location filename="previsions/evenements.cpp" line="170"/>
        <source>Ombre -&gt; Pénombre</source>
        <translation>Shadow -&gt; Penumbra</translation>
    </message>
    <message>
        <location filename="previsions/evenements.cpp" line="171"/>
        <source>Pénombre -&gt; Lumière</source>
        <translation>Penumbra -&gt; Light</translation>
    </message>
    <message>
        <location filename="previsions/evenements.cpp" line="171"/>
        <source>Lumière -&gt; Pénombre</source>
        <translation>Light -&gt; Penumbra</translation>
    </message>
    <message>
        <location filename="previsions/evenements.cpp" line="196"/>
        <source>Périgée :</source>
        <translation>Perigee :</translation>
    </message>
    <message>
        <location filename="previsions/evenements.cpp" line="196"/>
        <source>Apogée :</source>
        <translation>Apogee :</translation>
    </message>
    <message>
        <location filename="previsions/evenements.cpp" line="230"/>
        <source>Transition jour -&gt; nuit</source>
        <translation>Day -&gt; Night transition</translation>
    </message>
    <message>
        <location filename="previsions/evenements.cpp" line="231"/>
        <source>Transition nuit -&gt; jour</source>
        <translation>Night -&gt; Day transition</translation>
    </message>
    <message>
        <location filename="previsions/evenements.cpp" line="249"/>
        <source>Passage à PSO =</source>
        <translation>Pass to position =</translation>
    </message>
    <message>
        <location filename="previsions/evenements.cpp" line="304"/>
        <location filename="previsions/prevision.cpp" line="180"/>
        <source>  (numéro NORAD : %1)</source>
        <translation>  (NORAD number : %1)</translation>
    </message>
    <message>
        <location filename="previsions/evenements.cpp" line="306"/>
        <source>   Date      Heure      PSO    Longitude  Latitude  Évènements</source>
        <translation>   Date      Hour     Position Longitude  Latitude  Events</translation>
    </message>
    <message>
        <location filename="previsions/evenements.cpp" line="321"/>
        <location filename="previsions/flashs.cpp" line="281"/>
        <location filename="previsions/prevision.cpp" line="304"/>
        <location filename="previsions/transitiss.cpp" line="396"/>
        <source>Temps écoulé : %1s</source>
        <translation>Elapsed time : %1s</translation>
    </message>
    <message>
        <location filename="previsions/iridium.cpp" line="67"/>
        <source>ADGS</source>
        <translation>FRLS</translation>
    </message>
    <message>
        <location filename="previsions/flashs.cpp" line="226"/>
        <source>%1   Date       Heure    Azimut Sat Hauteur Sat  AD Sat    Decl Sat  Cst  Ang  Mir Magn   Alt   Dist  Az Soleil  Haut Soleil   Long Max    Lat Max     Distance  Magn Max</source>
        <translation>%1   Date        Hour    Sat Azimuth Sat Elev    RA Sat    Decl Sat  Cst  Ang  Mir Magn   Alt  Range   Sun Azim   Sun Elev     Max Long    Max Lat      Range    Max Magn</translation>
    </message>
    <message>
        <location filename="previsions/flashs.cpp" line="704"/>
        <location filename="previsions/transitiss.cpp" line="323"/>
        <source>(W)</source>
        <translation>(W)</translation>
    </message>
    <message>
        <location filename="previsions/flashs.cpp" line="704"/>
        <location filename="previsions/transitiss.cpp" line="319"/>
        <source>(E)</source>
        <translation>(E)</translation>
    </message>
    <message>
        <location filename="previsions/transitiss.cpp" line="284"/>
        <source>T</source>
        <translation>T</translation>
    </message>
    <message>
        <location filename="previsions/transitiss.cpp" line="284"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="previsions/transitiss.cpp" line="285"/>
        <source>L</source>
        <translation>M</translation>
    </message>
    <message>
        <location filename="previsions/transitiss.cpp" line="286"/>
        <source>Omb</source>
        <translation>Sha</translation>
    </message>
    <message>
        <location filename="previsions/transitiss.cpp" line="287"/>
        <source>Pen</source>
        <translation>Pen</translation>
    </message>
    <message>
        <location filename="previsions/transitiss.cpp" line="287"/>
        <source>Lum</source>
        <translation>Ill</translation>
    </message>
    <message>
        <location filename="previsions/transitiss.cpp" line="317"/>
        <source>(N)</source>
        <translation>(N)</translation>
    </message>
    <message>
        <location filename="previsions/transitiss.cpp" line="321"/>
        <source>(S)</source>
        <translation>(S)</translation>
    </message>
    <message>
        <location filename="previsions/transitiss.cpp" line="369"/>
        <source>   Date       Heure    Azimut Sat Hauteur Sat  AD Sat    Decl Sat  Cst  Ang  Type Corps Ill   Alt   Dist  Az Soleil  Haut Soleil    Long Max    Lat Max   Distance</source>
        <translation>   Date        Hour    Sat Azimuth Sat Elev    RA Sat    Decl Sat  Cst  Ang  Type Body  Ill   Alt  Range   Sun Azim    Sun Elev     Max Long    Max Lat     Range</translation>
    </message>
    <message>
        <location filename="librairies/corps/systemesolaire/planete.cpp" line="120"/>
        <source>Mercure</source>
        <translation>Mercury</translation>
    </message>
    <message>
        <location filename="librairies/corps/systemesolaire/planete.cpp" line="120"/>
        <source>Vénus</source>
        <translation>Venus</translation>
    </message>
    <message>
        <location filename="librairies/corps/systemesolaire/planete.cpp" line="120"/>
        <source>Mars</source>
        <translation>Mars</translation>
    </message>
    <message>
        <location filename="librairies/corps/systemesolaire/planete.cpp" line="120"/>
        <source>Jupiter</source>
        <translation>Jupiter</translation>
    </message>
    <message>
        <location filename="librairies/corps/systemesolaire/planete.cpp" line="121"/>
        <source>Saturne</source>
        <translation>Saturn</translation>
    </message>
    <message>
        <location filename="librairies/corps/systemesolaire/planete.cpp" line="121"/>
        <source>Uranus</source>
        <translation>Uranus</translation>
    </message>
    <message>
        <location filename="librairies/corps/systemesolaire/planete.cpp" line="121"/>
        <source>Neptune</source>
        <translation>Neptune</translation>
    </message>
    <message>
        <location filename="main.cpp" line="90"/>
        <source>Une instance de %1 est déjà ouverte</source>
        <translation>An instance of %1 is already launched</translation>
    </message>
    <message>
        <location filename="main.cpp" line="111"/>
        <source>Initialisation de la configuration...</source>
        <translation>Initialisation of configuration...</translation>
    </message>
    <message>
        <location filename="main.cpp" line="115"/>
        <source>Ouverture du fichier TLE...</source>
        <translation>Loading TLE file...</translation>
    </message>
    <message>
        <location filename="main.cpp" line="119"/>
        <source>Mise à jour des TLE...</source>
        <translation>Updating TLEs...</translation>
    </message>
    <message>
        <location filename="main.cpp" line="123"/>
        <source>Démarrage de l&apos;application...</source>
        <translation>Launching application...</translation>
    </message>
    <message>
        <location filename="librairies/dates/date.cpp" line="312"/>
        <source>dd/MM/yyyy</source>
        <translation>MM/dd/yyyy</translation>
    </message>
    <message>
        <location filename="librairies/dates/date.cpp" line="359"/>
        <source>dddd dd MMMM yyyy hh:mm:ss</source>
        <translation>dddd, MMMM dd yyyy  hh:mm:ss</translation>
    </message>
    <message>
        <location filename="librairies/dates/date.cpp" line="439"/>
        <source>Fichier %1 absent</source>
        <translation>File %1 missing</translation>
    </message>
    <message>
        <location filename="previsions/prevision.cpp" line="185"/>
        <source>   Date      Heure   Azimut Sat Hauteur Sat  AD Sat    Decl Sat  Const Magn  Altitude  Distance  Az Soleil   Haut Soleil</source>
        <translation>   Date      Hour   Sat Azimuth  Sat Elev    RA Sat    Decl Sat  Const Magn  Altitude     Range   Sun Azim    Sun Elev</translation>
    </message>
    <message>
        <location filename="previsions/metop.cpp" line="61"/>
        <source>FCB</source>
        <translation>FCB</translation>
    </message>
    <message>
        <location filename="librairies/corps/satellite/satellite.cpp" line="135"/>
        <location filename="librairies/corps/satellite/satellite.cpp" line="177"/>
        <source>AOS</source>
        <translation>AOS</translation>
    </message>
    <message>
        <location filename="librairies/corps/satellite/satellite.cpp" line="147"/>
        <source>LOS</source>
        <translation>LOS</translation>
    </message>
</context>
<context>
    <name>Telecharger</name>
    <message>
        <location filename="telecharger.ui" line="35"/>
        <source>Téléchargement de fichiers</source>
        <translation>File downloader</translation>
    </message>
    <message>
        <location filename="telecharger.ui" line="55"/>
        <source>Interroger le serveur...</source>
        <translation>Interrogate the server...</translation>
    </message>
    <message>
        <location filename="telecharger.ui" line="112"/>
        <source>Télécharger...</source>
        <translation>Download...</translation>
    </message>
    <message>
        <location filename="telecharger.ui" line="128"/>
        <source>Fermer</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="telecharger.cpp" line="213"/>
        <source>Erreur lors du téléchargement du fichier :
%1</source>
        <translation>Error while downloading file :
%1</translation>
    </message>
    <message>
        <location filename="telecharger.cpp" line="351"/>
        <source>Veuillez redémarrer %1 pour prendre en compte la mise à jour</source>
        <translation>Please restart %1 in order to take into account updates</translation>
    </message>
    <message>
        <location filename="telecharger.cpp" line="352"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
</context>
</TS>
