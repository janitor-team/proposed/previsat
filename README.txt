Description of available files :

- previsat3.x.y.z-exe-win32-64.zip          : Complete binaries for Windows (French, English)
- previsat3.x.y.z-bin-i386-Linux.deb.tar.gz : Complete binaries for Linux (French, English)
- previsat3.x.y.z-bin-x86_64-MacOS.dmg      : Complete binaries for MacOS X (French, English)
- previsat3.x.y.z-src.tar.gz                : Complete source code in tar.gz format
- previsat3.x.y.z-src.zip                   : Complete source code in zip format

- changes.txt                               : What changed since last revision
- license.txt                               : License of PreviSat software
- notice.txt                                : Notice of PreviSat software
- README.txt                                : This file


NOTE : Google Chrome or some other browsers may detect PreviSat as a malware. It is a false warning, PreviSat is not malicious for your computer!


WINDOWS INSTALL :
- Unzip previsat3.x.y.z-exe-win32-64.zip and run the install setup.

LINUX INSTALL (DEBIAN-LIKE DISTRIBUTIONS) :
- In the same directory of previsat3.x.y.z-bin-i386-Linux.deb.tar.gz, enter the following commands :
  tar -xfz previsat3.x.y.z-bin-i386-Linux.deb.tar.gz
  sudo dpkg -i previsat3.x.y.z-bin-i386-Linux.deb

MacOS X INSTALL :
- Open previsat3.x.y.z-bin-x86_64-MacOS.dmg and drag-and-drop PreviSat on the Applications shortcut.


NOTE : you must have the Adobe Flash Plugin (C) for Mozilla Firefox (C) in order to display the ISS Live video stream.


-----------------------

COMPILATION OF PREVISAT SOURCE CODE :

- Unpack the source package (for example tar -xfz previsat3.x.y.z-src.tar.gz),
- PreviSat is developed with Qt 4.8.7 but its source code is also compatible with Qt 4.8.6 and certainly with Qt 5.x
- Specify the zlib (1.2.8) directory in PreviSat.pro (download zlib at http://zlib.net/ if you don't have it on your system),
- Compile the source code with Qt Creator (or run the following commands : make clean; qmake -config release; make). When compiled in release mode with GCC, there are no warnings (or very little).
- By default, PreviSat is in French language. To have it in English language, run lrelease in order to generate the compiled language file. To have PreviSat in other language, you have to specify the PreviSat_xx.ts in PreviSat.pro, launch lupdate and complete the PreviSat_xx.ts with linguist, and finally run lrelease.


INSTALLATION OF PREVISAT AFTER COMPILATION :

To run PreviSat after the compilation, you need :
- Qt libraries
- zlib library (version 1.2.8)
- If you have generated it, a compiled translation file (must be in the same directory of executable)
- several directories specific to PreviSat :
    * doc  : contains user manual in pdf format (this directory must be in the same directory of the binary file PreviSat)

    * data : contains files necessary to PreviSat. This directory must be in your Data directory :
             -> LINUX PLATFORMS  : /usr/share/Astropedia/PreviSat/data : sub-directory "stars" and sub-directory sound (aos-default.wav and los-default)
                                   $HOME/.local/share/Astropedia/PreviSat/data for other sub-directories and their files
             -> WINDOWS PLATFORM : C:\Users\<user>\AppData\Local\Astropedia\PreviSat\data
             -> MacOS X PLATFORM : in the same directory of executable

    * tle  : contains TLE files (in the local Data directory for Windows and Linux, in the same directory of executable for Mac OS X)



-----------------------

UNINSTALL (when install setup was used) :

WINDOWS PLATFORM :
- It can be made by opening the Control Panel.

LINUX PLATFORMS (DEBIAN-LIKE DISTRIBUTIONS) :
- Launch the following command : sudo apt-get remove previsat

MacOS X PLATFORM :
- Move the application to the trash
