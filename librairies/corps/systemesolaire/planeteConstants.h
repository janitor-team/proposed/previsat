/*
 *     PreviSat, Satellite tracking software
 *     Copyright (C) 2005-2016  Astropedia web: http://astropedia.free.fr  -  mailto: astropedia@free.fr
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * _______________________________________________________________________________________________________
 *
 * Nom du fichier
 * >    planeteConstants.h
 *
 * Localisation
 * >    librairies.corps.systemesolaire
 *
 * Heritage
 * >
 *
 * Description
 * >     Constantes liees aux planetes
 *
 * Auteur
 * >    Astropedia
 *
 * Date de creation
 * >    21 juillet 2012
 *
 * Date de revision
 * >    3 juin 2015
 *
 */

#ifndef PLANETECONSTANTS_H
#define PLANETECONSTANTS_H

/* Enumerations */
enum IndicePlanete {
    MERCURE = 0,
    VENUS   = 1,
    MARS    = 2,
    JUPITER = 3,
    SATURNE = 4,
    URANUS  = 5,
    NEPTUNE = 6
};

/* Declaration des constantes */


#endif // PLANETECONSTANTS_H

